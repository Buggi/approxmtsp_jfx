package de.approxmtsp.ui;

import de.approxmtsp.controller.GraphDrawer;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class InsertIntegerValueBox {
    private static int value = -1;
    public static int display(String title, String message, boolean limitToVerticesCount) {
        Stage window = new Stage();
        //Block interaction with main window
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);

        Label labelInfo = new Label(message);
        final int max = limitToVerticesCount ? GraphDrawer.getVertices().size(): Integer.MAX_VALUE;
        Spinner<Integer> integerSpinner = new Spinner<>(1, max,1,1);
        integerSpinner.setEditable(true);

        Button btnOk = new Button("OK");
        Button btnCancel = new Button("cancel");

        btnOk.setOnAction(e -> {
            value = integerSpinner.getValue();
            window.close();
        });

        btnCancel.setOnAction(e -> window.close());

        HBox layout = new HBox(10);
        layout.getChildren().addAll(labelInfo, integerSpinner, btnOk, btnCancel);
        layout.setAlignment(Pos.TOP_LEFT);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        return value;
    }
}
