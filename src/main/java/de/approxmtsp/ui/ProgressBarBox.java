package de.approxmtsp.ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class ProgressBarBox {
  public static Stage display(String title, ProgressBar progressBar) {
    Stage window = new Stage();
    //Block interaction with main window
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle(title);
    window.setMinWidth(250);

    VBox layout = new VBox();
    layout.getChildren().addAll(progressBar);
//    layout.setAlignment(Pos.TOP_LEFT);

    Scene scene = new Scene(layout);
    window.setScene(scene);
    //        window.showAndWait();
    window.show();

    return window;
  }
}
