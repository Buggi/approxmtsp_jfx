package de.approxmtsp.ui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {
    private static boolean answer;

    public static boolean display(String title, String message) {
        Stage window = new Stage();
        //Block interaction with main window
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        Label label = new Label(message);

        Button btnYes = new Button("YES");
        Button btnNo = new Button("NO");

        btnYes.setOnAction(event -> {
            answer = true;
            window.close();
        });

        btnNo.setOnAction(event -> {
            answer = false;
            window.close();
        });

        HBox layout = new HBox(10);
        layout.getChildren().addAll(label, btnYes, btnNo);
        layout.setAlignment(Pos.TOP_LEFT);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}
