package de.approxmtsp.ui;

import de.approxmtsp.algorithms.ApproxAlgorithm;
import de.approxmtsp.algorithms.OptimizationAlgorithm;
import de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue.Mover;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MainApp extends Application {

    private final  static String DIRECTORY = "data/";
//    private static File file = null;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("approxmtsp.fxml"));
        primaryStage.setTitle("ApproxMTSP");
        primaryStage.setMaximized(true); //fullscreen
        primaryStage.setOnCloseRequest(event -> {
            //prevents from closing it; this application is now responsible for closing
//            event.consume();
            closeApplication();
        });
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        fillChoiceBoxAlgorithm(root);
        fillChoiceBoxOptimization(root);
        final ListView<File> listViewFiles = (ListView<File>) root.getScene().lookup("#listViewFiles");
        setCellFactory(listViewFiles);
        fillFileListView(listViewFiles);
        ScrollPane rootScrollPane = (ScrollPane) root.getScene().lookup("#rootScrollPane");
        StackPane zoomStackPane = (StackPane) root.getScene().lookup("#zoomStackPane");
        Pane contentPane = (Pane) root.getScene().lookup("#contentPane");
        ProgressBar progressBar = (ProgressBar) root.getScene().lookup("#progressBar");
        Label labelProgress = (Label) root.getScene().lookup("#labelProgress");

//        addListViewListener(listViewFiles, contentPane, rootScrollPane);

//        addGroupBoundsListener(zoomStackPane, contentGroup);
        addViewPortListener(rootScrollPane, zoomStackPane);
        labelProgress.textProperty().bind(progressBar.progressProperty().asString());
        listViewFiles.getSelectionModel().selectFirst();
        final ChoiceBox<String> choiceAlgorithm = (ChoiceBox<String>) root.getScene().lookup(("#choiceAlgorithm"));
        choiceAlgorithm.getSelectionModel().selectLast();
        final Spinner<Integer> maxYTriesSpinner = (Spinner<Integer>) root.getScene().lookup("#spinMaxYTries");
        Mover.MAX_Y_TRIES = maxYTriesSpinner.getValue();
    }

    private void addGroupBoundsListener(StackPane zoomStackPane, Group group) {
        group.layoutBoundsProperty().addListener((observable, oldBounds, newBounds) -> {
            //keep it at least as large as the content
            zoomStackPane.setMinWidth(newBounds.getWidth());
            zoomStackPane.setMinHeight(newBounds.getHeight());
        });
    }

    private void addViewPortListener(ScrollPane rootScrollPane, StackPane zoomStackPane) {
        rootScrollPane.viewportBoundsProperty().addListener((observable, oldBounds, newBounds) -> {
            // use viewport size, if not too small for zoomTarget
            zoomStackPane.setPrefSize(newBounds.getWidth(), newBounds.getHeight());
        });
    }

    private void fillChoiceBoxAlgorithm(Parent root) {
        final ChoiceBox checkBox = (ChoiceBox)root.lookup("#choiceAlgorithm");
        for (ApproxAlgorithm algorithm : ApproxAlgorithm.values()) {
            checkBox.getItems().add(algorithm.toString());
            checkBox.getSelectionModel().selectFirst();
        }
    }

    private void fillChoiceBoxOptimization(Parent root) {
        final ChoiceBox<String> checkBox = (ChoiceBox<String>)root.lookup("#choiceOptimization");
        for (OptimizationAlgorithm algorithm : OptimizationAlgorithm.values()) {
            checkBox.getItems().add(algorithm.toString());
            checkBox.getSelectionModel().selectFirst();
        }
    }

    private void setCellFactory(ListView<File> listViewFiles) {
        listViewFiles.setCellFactory(lv -> new ListCell<>() {
            @Override
            protected void updateItem(File file, boolean empty) {
                super.updateItem(file, empty);
                setText(file == null ? null : file.getName());
            }
        });
    }

    private void fillFileListView(ListView<File> listViewFiles) {
        Objects.requireNonNull(listViewFiles, "listViewFiles is null");
        File fileDirectory = new File("data");
        File[] files;
        if (null == (files = fileDirectory.listFiles())) return;

        List<File> fileList = Arrays.asList(files);
        final List<File> tspFiles = fileList.stream()
                .filter(file -> !file.isDirectory() && file.getName().endsWith(".tsp")).sorted(Comparator.comparingInt(this::extractIntegerOfFileName)).collect(Collectors.toList());
//        Descending order
//        Collections.reverse(tspFiles);
        listViewFiles.getItems().addAll(tspFiles);
    }

    private Integer extractIntegerOfFileName(File file) {
        return Integer.valueOf(file.getName().replaceAll("[^0-9]", ""));
    }


    private void closeApplication() {
//        boolean answer = ConfirmBox.display("Close Application", "Are you sure you want to close the application?");
//        if(answer) {
//            window.close();
//        }
    }
}
