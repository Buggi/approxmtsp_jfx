package de.approxmtsp.controller;

import de.approxmtsp.algorithms.ApproxAlgorithm;
import de.approxmtsp.algorithms.OptimizationAlgorithm;
import de.approxmtsp.algorithms.tourcalculator.*;
import de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue.Mover;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import de.approxmtsp.ui.InsertIntegerValueBox;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.logging.Logger;

import static de.approxmtsp.algorithms.support.CostCalculator.tourCostsRounded;
import static java.util.Objects.requireNonNull;

public class StageController implements Initializable {
    private static final double MAX_SCALE = 10.0d;
    private static final double MIN_SCALE = .1d;
    private static final String userDir = System.getProperty("user.home");
    private static final String INITIAL_DIRECTORY = userDir + "/Desktop";
    private static List<Integer> usedVertices = new ArrayList<>();
    private static String currentGraphName;
    @FXML
    StackPane zoomStackPane;
    @FXML
    ScrollPane rootScrollPane;
    @FXML
    StackPane contentStackPane;
    @FXML
    Group contentGroup;
    @FXML
    Pane contentPane;
    @FXML
    MenuBar menuBarMain;
    @FXML
    ProgressIndicator progressBar;
    @FXML
    CheckBox cbStepByStep;
    @FXML
    CheckBox cbAnimationEnabled;
    @FXML
    ChoiceBox choiceAlgorithm;
    @FXML
    ChoiceBox choiceOptimization;
    @FXML
    Label labelCalcTime;
    @FXML
    Label labelCosts;
    @FXML
    Label labelCostsRounded;
    @FXML
    Label labelProgress;
    @FXML
    Label labelStartNode;
    @FXML
    Label labelOptimum;
    @FXML
    ListView<File> listViewFiles;
    @FXML
    CheckBox checkBoxRandomVertices;
    @FXML
    CheckBox checkBoxMinMaxRandomNodes;
    @FXML
    Spinner<Integer> spinMaxYTries;
    @FXML
    Spinner<Integer> spinRetries;

    private AbstractRouteCalculator calculator;
    private final static Logger logger = Logger.getLogger(StageController.class.getName());
    private boolean calculateAll;
    private File tempFile;
    private boolean calculateFromMinMaxAndAverageStartNode;
    private boolean calculateWithFiftyRandomStartVertices;
    private static int currentStartVertex = -1;
    private static double currentBestCosts = Double.MAX_VALUE;
    private boolean minMaxRandCalculationIsFinished;
    private boolean useAllAlgorithms;
    private boolean noFurtherCalculation;
    private static int amountRandomStartVertices = 5;
    private AbstractRouteCalculator optimizer;
    private final ObservableList<Edge> tourEdges = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tourEdges.addListener((InvalidationListener) observable -> Platform.runLater(() -> {
            synchronized (tourEdges) {
                GraphDrawer.redrawEdges(tourEdges);
            }
        }));
        addListViewListener();
    }


    public void handleMouseScrollZoomPane(ScrollEvent event) {
// source:        https://stackoverflow.com/a/38719541/10263863
        if (event.isControlDown()) {
            event.consume();
            final double zoomFactor = event.getDeltaY() > 0 ? 1.08 : 1 / 1.08;
            Bounds groupBounds = contentGroup.getLayoutBounds();
            final Bounds viewportBounds = rootScrollPane.getViewportBounds();
            // calculate pixel offsets from [0, 1] range
            double valX = rootScrollPane.getHvalue() * (groupBounds.getWidth() - viewportBounds.getWidth());
            double valY = rootScrollPane.getVvalue() * (groupBounds.getHeight() - viewportBounds.getHeight());
            // convert content coordinates to zoomTarget coordinates
            Point2D posInZoomTarget = contentStackPane.parentToLocal(contentGroup.parentToLocal(new Point2D(event.getX(), event.getY())));
            // calculate adjustment of scroll position (pixels)
            Point2D adjustment = contentStackPane.getLocalToParentTransform().deltaTransform(posInZoomTarget.multiply(zoomFactor - 1));
            // do the resizing
            contentStackPane.setScaleX(zoomFactor * contentStackPane.getScaleX());
            contentStackPane.setScaleY(zoomFactor * contentStackPane.getScaleY());
            // keeps children's size
            scaleContentPanesChildren(1 / zoomFactor);
            // refresh ScrollPane scroll positions & content bounds
            rootScrollPane.layout();
            // convert back to [0, 1] range
            // (too large/small values are automatically corrected by ScrollPane)
            groupBounds = contentGroup.getLayoutBounds();
            rootScrollPane.setHvalue((valX + adjustment.getX()) / (groupBounds.getWidth() - viewportBounds.getWidth()));
            rootScrollPane.setVvalue((valY + adjustment.getY()) / (groupBounds.getHeight() - viewportBounds.getHeight()));
        }
    }

    private void scaleContentPanesChildren(double delta) {
        scaleAllNodes(delta);
        scaleAllEdges(delta);
    }

    private void scaleAllEdges(double delta) {
        contentPane.getChildren()
                .stream()
                .filter(Line.class::isInstance)
                .forEach(line -> ((Line) line).setStrokeWidth(((Line) line).getStrokeWidth() * delta));
    }

    private void scaleAllNodes(double delta) {
        contentPane.getChildren()
                .stream()
                .filter(Circle.class::isInstance)
                .forEach(node -> {
                    node.setScaleX(node.getScaleX() * delta);
                    node.setScaleY(node.getScaleY() * delta);
                });
    }

    public void handleMenuLoad() throws FileNotFoundException {
        Stage stage = (Stage) menuBarMain.getScene().getWindow();
        FileChooser chooser = createFileChooser("Load tour or graph", new FileChooser.ExtensionFilter("supported files", "*.tour", "*.tsp"));
        File file = chooser.showOpenDialog(stage);
        if (null == file) return;

        final String[] splitFile = file.getPath().split("\\.");
        final String extension = splitFile[splitFile.length - 1];
        if ("tsp".equals(extension)) {
            GraphDrawer.removeAllEdges();
            listViewFiles.getItems().add(file);
            listViewFiles.getSelectionModel().select(file);
        } else if ("tour".equals(extension)) {
            GraphDrawer.removeAllEdges();
            tempFile = file;
            showTour(file);
        }
    }

    public void handleMenuSave() throws IOException {
        Stage stage = (Stage) menuBarMain.getScene().getWindow();
        FileChooser chooser = createFileChooser("Save tour",
                new FileChooser.ExtensionFilter("tour file", "*.tour"),
                new FileChooser.ExtensionFilter("comma separated tour file", "*.csv"),
                new FileChooser.ExtensionFilter("image file (png)", "*.png"));
        final File selectedItem = (File) listViewFiles.getSelectionModel().getSelectedItem();
        final String cleanFilename = selectedItem.getName().substring(0, selectedItem.getName().lastIndexOf("."));
        final int currentStartVertex = getCurrentStartVertex() + 1;
        chooser.setInitialFileName(cleanFilename + "_" + choiceAlgorithm.getSelectionModel().getSelectedItem().toString() + "_StartNode_" + currentStartVertex);
        File file = chooser.showSaveDialog(stage);

        if (null == file || null == tempFile) return;
        final String fileName = file.getName();

        FileUtils.copyFile(tempFile, file);
        if (fileName.endsWith(".csv")) {
            final File csvFile = requireNonNull(convertTourToCsv(tempFile));
            FileUtils.copyFile(csvFile, file);
        }
        if (fileName.endsWith(".png")) {
            try {
                //Pad the capture area
                WritableImage writableImage = new WritableImage((int) zoomStackPane.getWidth() + 20,
                        (int) zoomStackPane.getHeight() + 20);
                zoomStackPane.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                //Write the snapshot to the chosen file
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private FileChooser createFileChooser(String title, FileChooser.ExtensionFilter... extensionFilter) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(title);
        chooser.setInitialDirectory(new File(INITIAL_DIRECTORY));
        chooser.getExtensionFilters().addAll(extensionFilter);
        return chooser;
    }

    private File convertTourToCsv(File tempFile) throws IOException {
        File convertedCsv = File.createTempFile("csvConverted-", ".csv");
        PrintWriter writer = new PrintWriter(convertedCsv);
        Scanner scanner = new Scanner(tempFile);
        boolean reachedTourData = false;
        while (scanner.hasNextLine()) {
            String line;
            if (!reachedTourData && !"TOUR_SECTION".equals((line = scanner.nextLine()))) {
                writer.println(line);
                continue;
            }
            reachedTourData = true;
            String id;
            if ((id = scanner.nextLine()).equals("-1")) {
                writer.println(id);
                break;
            }
            final Optional<Vertex> vertex = GraphDrawer.getVertices().stream()
                    .filter(v -> v.getId() == Integer.valueOf(id))
                    .findFirst();
            if (!vertex.isPresent()) {
                //TODO: throw exception
                logger.warning("ERROR: Vertex with the following id doesn't exist currently: " + id);
                return null;
            }
            writer.println(vertex.get().getId() + ";" + vertex.get().getX() + ";" + vertex.get().getY());
        }
        writer.close();
        return convertedCsv;
    }

    private void showTour(File file) throws FileNotFoundException {
        List<Integer> verticesIds = new ArrayList<>();
        Scanner scanner = new Scanner(file);
        boolean reachedTourData = false;
        while (scanner.hasNextLine()) {
            if (!reachedTourData && !"TOUR_SECTION".equals(scanner.nextLine())) continue;
            reachedTourData = true;
            String id;
            if ((id = scanner.nextLine()).equals("-1")) {
                break;
            }
            verticesIds.add(Integer.valueOf(id));
        }
        if (!reachedTourData) return;
        final List<Edge> tour = GraphDrawer.insertTour(verticesIds);
        calculateAndShowCosts(tour);
        calculateAndShowCostsRounded(tour);
    }


    public void handleMenuDelete() {
        GraphDrawer.clearContentPane();
    }

    public void handleZoomOutClick() {
//        final double delta = 1.1;
//        zoomPane.getChildren().forEach(node -> {
//            Shape s = (Shape) node;
//            s.setStrokeWidth(s.getStrokeWidth() / delta);
//        });
        GraphDrawer.getVertices().forEach(v -> v.setRadius(v.getRadius() * 0.9));
        GraphDrawer.redrawVertices();
    }

    public void handleZoomInClick() {
//        final double delta = 1.1;
//        zoomPane.getChildren().forEach(node -> {
//            Shape s = (Shape) node;
//            s.setStrokeWidth(s.getStrokeWidth() * delta);
//        });
        GraphDrawer.getVertices().forEach(v -> v.setRadius(v.getRadius() * 1.1));
        GraphDrawer.redrawVertices();
    }

    public void handlePause() {
        if (AbstractStepByStepCalculator.isPaused()) {
            if (null != calculator)
                calculator.resume();
            if (null != optimizer)
                optimizer.resume();
        }
        AbstractStepByStepCalculator.pause();
    }

    public void handleStop() {
        GraphDrawer.removeAllEdges();
        AbstractStepByStepCalculator.stop();
    }

    public void handleNext() {
        if (null != calculator)
            calculator.resume();
        if (null != optimizer)
            optimizer.resume();
    }

    public void handleAnimationEnabled() {
        AbstractRouteCalculator.setAnimationEnabled(cbAnimationEnabled.isSelected());
    }

    public void handleSpinnerMaxTriesClick() {
        Mover.MAX_Y_TRIES = spinMaxYTries.getValue();
    }

    public void handleSpinnerRetriesChanged() {
        if (spinRetries.getValue() == null)
            return;
        LinKernighanSpanningTreeValueTourCalculator.MAX_TRIES = spinRetries.getValue();
    }

    public void startTour() {
        try {
            currentBestCosts = Double.MAX_VALUE;
            minMaxRandCalculationIsFinished = false;
            if (0 == listViewFiles.getSelectionModel().getSelectedItems().size()) return;
            if (calculateFromMinMaxAndAverageStartNode) {
                startCalculating(GraphDrawer.getMinNode());
            } else if (calculateWithFiftyRandomStartVertices) {
                usedVertices = new ArrayList<>();
                int randomVertex = getUnusedRandomVertex();
                startCalculating(randomVertex);
            } else {
                startCalculating(0);
            }
        } catch (Exception e) {
            final Throwable cause = e.getCause();
            String causeMsg = null != cause ? cause.getMessage() : "no cause message available";
            AlertBox.display("Exception occurred", "Exception message: " + e.getMessage() + "\n cause: " + causeMsg);
        }
    }

    private void startCalculating(int startNode) throws IOException, InterruptedException {
        setCurrentStartVertex(startNode);
        labelStartNode.setText(startNode + 1 + "");
        GraphDrawer.removeAllEdges();
        final List<Vertex> vertices = GraphDrawer.getVertices();
        if (startNode > vertices.size()) return;//TODO: throw exception
//        tourEdges = FXCollections.observableList(new ArrayList<>());
        calculator = createCalculator(vertices, tourEdges);
        final CalculatorService calculatorService = new CalculatorService(calculator, startNode);
        final long startTime = System.currentTimeMillis();
        calculatorService.start();
        progressBar.setVisible(true);
        progressBar.progressProperty().bind(calculatorService.progressProperty());
        optimizer = createOptimizer(GraphDrawer.getVertices(), tourEdges);
        final CalculatorService optimizerService = new CalculatorService(optimizer, startNode);
        calculatorService.setOnSucceeded(event -> {
            progressBar.setVisible(false);
            //TODO: CheckBox "optimize afterwards" or smth like that
//            if (null != optimizer) {
//                optimizerService.start();
//            }
            final long calculationTime = calculateAndShowCalculationTime(startTime);
            final double calculationCosts = calculateAndShowCosts(tourEdges);
            final double calculationCostsRounded = calculateAndShowCostsRounded(tourEdges);
            final String graphName = getCurrentGraphName();
            createTempTourFile(calculatorService.getValue(), calculationCosts, calculationCostsRounded, graphName);
            addToComparisonTable(startNode, calculationTime, calculationCostsRounded, graphName, "ComparisonRounded.xlsx");
            addToComparisonTable(startNode, calculationTime, calculationCosts, graphName, "Comparison.xlsx");
            try {
                selectNextItemOrNextAlgorithmAndRepeatCalculation(startNode);
            } catch (InterruptedException | IOException e) {
                AlertBox.display("Error during calculation", e.getMessage());
                ;
                e.printStackTrace();
            }
        });
        optimizerService.setOnSucceeded(event -> {
            final long calculationTime = calculateAndShowCalculationTime(startTime);
            final double calculationCosts = calculateAndShowCosts(tourEdges);
            final double calculationCostsRounded = calculateAndShowCostsRounded(tourEdges);
            final String graphName = getCurrentGraphName();
            createTempTourFile(optimizerService.getValue(), calculationCosts, calculationCostsRounded, graphName);
            addToComparisonTable(startNode, calculationTime, calculationCostsRounded, graphName, "ComparisonOptimizationRounded.xlsx");
            addToComparisonTable(startNode, calculationTime, calculationCosts, graphName, "ComparisonOptimization.xlsx");
        });
    }

    public void startOptimizing() throws IOException, InterruptedException {
        optimizer = createOptimizer(GraphDrawer.getVertices(), tourEdges);
        if (null == optimizer)
            return;
        final CalculatorService optimizerService = new CalculatorService(optimizer, 0);
        final long startTime = System.currentTimeMillis();
        optimizerService.start();
        progressBar.setVisible(true);
        progressBar.progressProperty().bind(optimizerService.progressProperty());
        optimizerService.setOnSucceeded(event -> {
            progressBar.setVisible(false);
            final long calculationTime = calculateAndShowCalculationTime(startTime);
            final double calculationCosts = calculateAndShowCosts(GraphDrawer.getEdges());
            final double calculationCostsRounded = calculateAndShowCostsRounded(GraphDrawer.getEdges());
            final String graphName = getCurrentGraphName();
            createTempTourFile(optimizerService.getValue(), calculationCosts, calculationCostsRounded, graphName);
            addToComparisonTable(-1, calculationTime, calculationCostsRounded, graphName, "ComparisonOptimizationRounded.xlsx");
            addToComparisonTable(-1, calculationTime, calculationCosts, graphName, "ComparisonOptimization.xlsx");
        });
    }

    private void addToComparisonTable(int startNode, long calculationTime, double calculationCosts, String graphName, String filePath) {
        createAndExportComparisonTable(graphName, calculationTime, calculationCosts, startNode + 1, filePath);
        if (calculationCosts < currentBestCosts) {
            currentBestCosts = calculationCosts;
        }
    }

    private void selectNextItemOrNextAlgorithmAndRepeatCalculation(int startNode) throws InterruptedException, IOException {
        int selectedIndex;
        if (noFurtherCalculation) return;
        if (calculateAll) {
            if (calculateFromMinMaxAndAverageStartNode && !minMaxRandCalculationIsFinished) {
                startFromMinMaxOrRandomNode(startNode);
            } else if (calculateWithFiftyRandomStartVertices && usedVertices.size() < amountRandomStartVertices - 1 && usedVertices.size() < GraphDrawer.getVertices().size()) {
                int randomNode = getUnusedRandomVertex();
                usedVertices.add(randomNode);
                startCalculating(randomNode);
            } else if (useAllAlgorithms && choiceAlgorithm.getItems().size() - 1 > (selectedIndex = choiceAlgorithm.getSelectionModel().getSelectedIndex())) {
                startWithNextAlgorithm(selectedIndex);
            } else if (listViewFiles.getItems().size() - 1 > (selectedIndex = listViewFiles.getSelectionModel().getSelectedIndex())) {
                minMaxRandCalculationIsFinished = false;
                usedVertices = new ArrayList<>();
                currentBestCosts = Double.MAX_VALUE;
                listViewFiles.getSelectionModel().select(selectedIndex + 1);
                if (useAllAlgorithms) choiceAlgorithm.getSelectionModel().select(0);
                startCalculating(0);
            }
        } else if (calculateFromMinMaxAndAverageStartNode && !minMaxRandCalculationIsFinished) {
            startFromMinMaxOrRandomNode(startNode);
//            if (useAllAlgorithms && choiceAlgorithm.getItems().size() - 1 > (selectedIndex = choiceAlgorithm.getSelectionModel().getSelectedIndex())) {
//                minMaxRandCalculationIsFinished = false;
//                startWithNextAlgorithm(selectedIndex);
//            }
        } else if (calculateWithFiftyRandomStartVertices && usedVertices.size() < amountRandomStartVertices - 1 && usedVertices.size() < GraphDrawer.getVertices().size()) {
            int randomNode = getUnusedRandomVertex();
            usedVertices.add(randomNode);
            startCalculating(randomNode);
        } else if (useAllAlgorithms && choiceAlgorithm.getItems().size() - 1 > (selectedIndex = choiceAlgorithm.getSelectionModel().getSelectedIndex())) {
            startWithNextAlgorithm(selectedIndex);
        } else {
            usedVertices = new ArrayList<>();
        }
    }

    private int getUnusedRandomVertex() {
        Random random = new Random();
        int randomNode;
        do {
            randomNode = random.nextInt(GraphDrawer.getVertices().size());
        }
        while (usedVertices.contains(randomNode));
        return randomNode;
    }


    private void startWithNextAlgorithm(int selectedIndex) throws IOException, InterruptedException {
        currentBestCosts = Double.MAX_VALUE;
        choiceAlgorithm.getSelectionModel().select(selectedIndex + 1);
//        listViewFiles.getSelectionModel().select(0);
        if (calculateFromMinMaxAndAverageStartNode) {
            minMaxRandCalculationIsFinished = false;
            startCalculating(GraphDrawer.getMinNode());
        } else if (calculateWithFiftyRandomStartVertices) {
            usedVertices = new ArrayList<>();
            int randomVertex = getUnusedRandomVertex();
            startCalculating(randomVertex);
        } else {
            startCalculating(0);
        }
    }

    private void startFromMinMaxOrRandomNode(int startNode) throws IOException, InterruptedException {
        if (GraphDrawer.getMinNode() == startNode) {
            startCalculating(GraphDrawer.getMaxNode());
        } else if (GraphDrawer.getMaxNode() == startNode) {
            minMaxRandCalculationIsFinished = true;
//            startCalculating(GraphDrawer.getRandomNode());
            startCalculating(GraphDrawer.getAverageNode());
        }
    }

    private void createAndExportComparisonTable(String graphName, long duration, double calculationCosts, int startNode, String path) {
        final int algorithmNumber = choiceAlgorithm.getSelectionModel().getSelectedIndex();
        final String algorithmName = choiceAlgorithm.getItems().get(algorithmNumber).toString();
        File comparisonFile = new File(path);
        try {
            comparisonFile.createNewFile(); // will do nothing if it already exists
            InputStream inputStream = new FileInputStream(path);
            Workbook workbook;
            if (0 < comparisonFile.length()) {
                workbook = new XSSFWorkbook(inputStream);
            } else {
                workbook = new XSSFWorkbook();
            }
            Sheet sheet = createOrGetSheet(workbook);

            boolean rowAvailable = false;
            boolean valueWasSet = false;
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                final Row row = sheet.getRow(i);
                if (null == row) continue;
                if (graphName.equals(row.getCell(0).getStringCellValue())) {
                    valueWasSet = setCellValue(calculationCosts, duration, startNode, algorithmNumber, row);
                    rowAvailable = true;
                    break;
                }
            }
            if (!rowAvailable) {
                Row row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue(graphName);
                valueWasSet = setCellValue(calculationCosts, duration, startNode, algorithmNumber, row);
            }
            inputStream.close();
            if (valueWasSet) {
                createTourFile(graphName, algorithmName, path, workbook, startNode);
            }
        } catch (IOException e) {
            logger.warning("Error during export comparison data: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void createTourFile(String graphName, String algorithmName, String path, Workbook workbook, int startNode) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        workbook.write(outputStream);
        outputStream.close();
        Path tourFilePath = Paths.get("tours" + File.separator + graphName.substring(0, graphName.lastIndexOf('.')) + algorithmName + "_StartNode_" + startNode + ".tour");
        Files.createDirectories(tourFilePath.getParent());
        if (Files.exists(tourFilePath)) {
            Files.delete(tourFilePath);
        }
        Files.copy(tempFile.toPath(), tourFilePath);
    }

    private Sheet createOrGetSheet(Workbook workbook) {
        if (0 == workbook.getNumberOfSheets()) {
            return createHeaderRow(workbook);
        }
        return workbook.getSheetAt(0);
    }

    private Sheet createHeaderRow(Workbook workbook) {
        Sheet sheet = workbook.createSheet();
        Row headerRow = sheet.createRow(0);
        String[] columns = {"Graph", "Nearest-Neighbour", "Start NN", "Double-Ended-Nearest-Neighbour", "Start DENN", "Nearest-Insertion", "Start NI", "Farthest-Insertion I", "Start FI-I", "Farthest-Insertion II", "Start FI-II", "Cheapest-Insertion", "Start CI", "Spanning-Tree", "Start ST", "Christofides", "Start CHR",
                "NN-Duration", "DENN-Duration", "NI-Duration", "FI-I-Duration", "FI-II-Duration", "CI-Duration", "ST-Duration", "CHR-Duration"};
        for (int i = 0; i < columns.length; i++) {
            headerRow.createCell(i).setCellValue(columns[i]);
        }
        return sheet;
    }

    private boolean setCellValue(double calculationCosts, double duration, int startNode, int algorithmNumber, Row row) {
        final Cell cell = row.getCell(2 * algorithmNumber + 1);
        if (null == cell || calculationCosts < cell.getNumericCellValue()) {
            row.createCell(2 * algorithmNumber + 1).setCellValue(calculationCosts);
            row.createCell(2 * algorithmNumber + 2).setCellValue(startNode);
            row.createCell(algorithmNumber + (choiceAlgorithm.getItems().size() * 2) + 1/*15*/).setCellValue(duration);
            return true;
        }
        return false;
    }

    private void createTempTourFile(List<Vertex> vertices, double calculationCosts, double calculationCostsRounded, String graphName) {
        PrintWriter writer = null;
        try {
            tempFile = File.createTempFile("tempTourFile-", ".tour");
            writer = new PrintWriter(tempFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tempFile.deleteOnExit();
        writer.println("NAME: " + graphName);
        writer.println("COMMENT: Length = " + calculationCosts);
        writer.println("COMMENT: Length (rounded) = " + calculationCostsRounded);
        writer.println("TYPE: TOUR");
        writer.println("DIMENSION: " + vertices.size());
        writer.println("TOUR_SECTION");
        PrintWriter finalWriter = writer;
        vertices.forEach(vertex -> finalWriter.println(vertex.getId()));
        writer.println("-1");
        writer.println("EOF");
        writer.close();
    }

    private long calculateAndShowCalculationTime(long startTime) {
        final long timeElapsed = System.currentTimeMillis() - startTime;
        String calcTime = timeElapsed < 1000 ? timeElapsed + "ms" : timeElapsed / 1000.00 + " seconds";
        labelCalcTime.setText(calcTime);
        return timeElapsed;
    }


    private double calculateAndShowCosts(List<Edge> tourList) {
        if (tourList.isEmpty()) return Double.MAX_VALUE;
        final double finalCosts = tourList.stream()
                .mapToDouble(edge -> euclideanDistance(edge.getSource(), edge.getTarget()))
                .sum();

        setTextOfCostsLabel(finalCosts, labelCosts);
        return finalCosts;
    }

    private double calculateAndShowCostsRounded(List<Edge> tourList) {
        final int finalCosts = tourCostsRounded(tourList);
//        setTextOfCostsLabel(finalCosts, labelCostsRounded);
        labelCostsRounded.setText(finalCosts + "");
        return finalCosts;
    }

    private void setTextOfCostsLabel(double finalCosts, Label label) {
        DecimalFormat decimalFormat = new DecimalFormat("###,##0.000");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        decimalFormat.setDecimalFormatSymbols(symbols);
        label.setText(decimalFormat.format(finalCosts));
    }

    private double euclideanDistance(Vertex v1, Vertex v2) {
        double xPowed = Math.pow(v2.getX() - (v1.getX()), 2);
        double yPowed = Math.pow(v2.getY() - (v1.getY()), 2);

        return Math.sqrt(xPowed + yPowed);
    }


    private AbstractRouteCalculator createCalculator(List<Vertex> vertices, ObservableList<Edge> tourEdges) throws IOException {
        final ApproxAlgorithm algorithm = ApproxAlgorithm.valueOf(choiceAlgorithm.getSelectionModel().getSelectedItem().toString());
        switch (algorithm) {
            case NEAREST_NEIGHBOUR:
                return new NearestNeighbourCalculator(vertices, tourEdges);
            case DOUBLE_ENDED_NEAREST_NEIGHBOUR:
                return new DoubleEndedNearestNeighbourCalculator(vertices, tourEdges);
            case NEAREST_INSERTION:
                return new NearestInsertionCalculator(vertices, tourEdges);
            case FARTHEST_INSERTION:
                return new FarthestInsertionCalculator(vertices, tourEdges);
            case FARTHEST_INSERTION_LOWEST_DISTANCE_MAX:
                return new FarthestInsertionLowestDistanceMaxCalculator(vertices, tourEdges);
            case CHEAPEST_INSERTION:
                return new CheapestInsertionCalculator(vertices, tourEdges);
            case SPANNING_TREE:
                return new SpanningTreeTourCalculator(vertices, tourEdges);
            case CHRISTOFIDES:
                return new ChristofidesCalculator(vertices, tourEdges);
            case RANDOM:
                return new RandomTourCalculator(vertices, tourEdges);
        }
        return null;
    }

    private AbstractRouteCalculator createOptimizer(List<Vertex> vertices, ObservableList<Edge> tourEdges) throws IOException, InterruptedException {
        final OptimizationAlgorithm algorithm = OptimizationAlgorithm.valueOf(choiceOptimization.getSelectionModel().getSelectedItem().toString());
        switch (algorithm) {
            case LIN_KERNIGHAN_SPANNING_VALUE:
                return new LinKernighanSpanningTreeValueTourCalculator(vertices, tourEdges);
        }
        return null;
    }


    public void handleMenuStepByStep() {
        AbstractRouteCalculator.setStepByStep(cbStepByStep.isSelected());
    }

    public void calculateAll(ActionEvent event) {
        calculateAll = ((CheckBox) event.getSource()).isSelected();
    }

    public void addCurrentTourToComparisonTable(ActionEvent event) {
        //TODO:
    }

    public void calculateStartFromMinMaxAndAverageNode(ActionEvent event) {
        minMaxRandCalculationIsFinished = false;
        boolean selected = ((CheckBox) event.getSource()).isSelected();
        calculateFromMinMaxAndAverageStartNode = selected;
        calculateWithFiftyRandomStartVertices = !selected;
        checkBoxRandomVertices.setSelected(!selected);
    }

    public void calculateFromFiftyRandomVertices(ActionEvent event) {
        currentStartVertex = 0;
        boolean selected = ((CheckBox) event.getSource()).isSelected();
        calculateWithFiftyRandomStartVertices = selected;
        calculateFromMinMaxAndAverageStartNode = !selected;
        checkBoxMinMaxRandomNodes.setSelected(!selected);
    }

    public void calculateAllAlgorithms(ActionEvent event) {
        useAllAlgorithms = ((CheckBox) event.getSource()).isSelected();
    }

    public static int getCurrentStartVertex() {
        return currentStartVertex;
    }

    public static void setCurrentStartVertex(int currentStartVertex) {
        StageController.currentStartVertex = currentStartVertex;
    }

    public void handleMenuStartWithSelected() throws IOException, InterruptedException {
        if (-1 == currentStartVertex) {
            AlertBox.display("No Vertex selected", "You need to select a vertex by performing point and click on it");
            return;
        }
        //TODO: is not working
        this.noFurtherCalculation = true;
        startCalculating(currentStartVertex);
        this.noFurtherCalculation = false;

    }

    public void handleMenuSelectVertex() {
        String message = "Please select an id (1-" + GraphDrawer.getVertices().size() + ")";
        final int startVertexId = InsertIntegerValueBox.display("Select Vertex by id", message, true);
        setCurrentStartVertex(startVertexId - 1);
        Optional<Vertex> startVertex = GraphDrawer.getVertices().stream()
                .filter(v -> v.getId() == startVertexId)
                .findFirst();
        if (startVertex.isPresent()) {
            GraphDrawer.getVertices().forEach(vertex -> vertex.getGraphic().setFill(Color.RED));
            startVertex.get().getGraphic().setFill(Color.BLACK);
        }

    }

    public void handleSetRandomVerticesAmount() {
        amountRandomStartVertices = InsertIntegerValueBox.display("Amount Of Random Start Vertices", "Select the amount of the random start vertices: ", false);

    }

    public void handleListItemDelete() {
        listViewFiles.getSelectionModel().getSelectedItems().forEach(item -> listViewFiles.getItems().remove(item));
    }

    public void handleReloadClick() {
        GraphDrawer.redrawEdges();
        calculateAndShowCosts(GraphDrawer.getEdges());
        calculateAndShowCostsRounded(GraphDrawer.getEdges());
    }

    public static void setCurrentGraphName(String graphName) {
        currentGraphName = graphName;
    }

    public static String getCurrentGraphName() {
        return currentGraphName;
    }

    private void addListViewListener() {
        listViewFiles.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                GraphDrawer.addVertices(contentPane, rootScrollPane, newValue, labelOptimum);
                GraphDrawer.setRandomNode();
                setCurrentStartVertex(-1);
                setCurrentGraphName(listViewFiles.getSelectionModel().getSelectedItem().getName());
                if(null != calculator) {
                    calculator.clearObservableEdges();
                }
                if (null != optimizer) {
                    optimizer.clearObservableEdges();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

    }
}
