package de.approxmtsp.controller;

import de.approxmtsp.algorithms.RouteCalculatorException;
import de.approxmtsp.algorithms.tourcalculator.AbstractRouteCalculator;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.ArrayList;
import java.util.List;

public class CalculatorService extends Service<List<Vertex>> {

    private AbstractRouteCalculator calculator;
    private int startNode;

    public CalculatorService(AbstractRouteCalculator calculator, int startNode) {
        this.calculator = calculator;
        this.startNode = startNode;
    }

    @Override
    protected Task<List<Vertex>> createTask() {
        return new Task<>() {
            @Override
            protected List<Vertex> call() {
                List<Vertex> route = new ArrayList<>();
                try {
                    route = calculator.calculate(startNode);
                } catch (RouteCalculatorException e) {
                    e.printStackTrace();
                    Platform.runLater(() -> AlertBox.display("Calculator Error", "Could not calculate Task\n" + e.getMessage()));
                    throw e;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Platform.runLater(() -> AlertBox.display("Calculator Error - InterruptedException", "Could not calculate Task\n" + e.getMessage()));
                } catch (Exception e) {
                    e.printStackTrace();
                    Platform.runLater(() -> AlertBox.display("Calculator Error - EXCEPTION", "Could not calculate Task\n" + e.getMessage()));
                    throw e;
                }
                return route;
            }
        };
    }
}
