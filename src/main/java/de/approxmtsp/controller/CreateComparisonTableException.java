package de.approxmtsp.controller;

public class CreateComparisonTableException extends Throwable {
    public CreateComparisonTableException(String message) {
        super(message);
    }
}
