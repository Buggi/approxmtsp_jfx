package de.approxmtsp.controller;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class GraphDrawer {

    private static List<Vertex> vertices = new ArrayList<>();
    private static final List<Edge> edges = new ArrayList<>();
    private static ScrollPane rootScrollPane = null;
    private static Pane contentPane = null;
    private static double minX;
    private static double minY;
    private static double xRatio;
    private static double yRatio;
    private static Logger logger = Logger.getLogger(GraphDrawer.class.getName());
    private static int randomNode;
    private static Long optimum = Long.MIN_VALUE;

    public static List<Vertex> getVertices() {
        return vertices;
    }

    public static Long getOptimum() {
        return optimum;
    }

    public static List<Edge> getEdges() {
        return edges;
    }

    public static void addVertices(Pane content, ScrollPane rootPane, File file, Label labelOptimum) throws FileNotFoundException {
        requireNonNull(content, "clearContentPane is null");
        requireNonNull(file, "file is null");
        requireNonNull(rootPane, "rootPane is null");
        vertices = new ArrayList<>();
        contentPane = content;
        rootScrollPane = rootPane;
        clearContentPane();
        scanFile(file, labelOptimum);
        addVertices();
        calculateMinMaxRangeRatio();
        rotateContentPane();
        removePreviousScaling();
        scaleContentPane();
    }

    private static void scanFile(File file, Label labelOptimum) throws FileNotFoundException {
        if (!isEucleadian2D(file)) {
//            throw new IllegalArgumentException()
            AlertBox.display("One file does not contain Eucleadian 2D coordinates",
                    "The following file does not contain Eucleadian 2D Points: " + file.getName());
        }
        readOptimum(file);
        labelOptimum.setText(optimum.toString());
        readCoordinates(file);
    }

    private static boolean isEucleadian2D(File file) throws FileNotFoundException {
        final Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(line.startsWith("EDGE_WEIGHT_TYPE")) {
                final String[] split = line.split(":");
                if (split[1].trim().equals("EUC_2D")) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void readOptimum(File file) throws FileNotFoundException {
        final Scanner scanner = new Scanner(file);
        optimum = Long.MIN_VALUE;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(line.startsWith("OPTIMUM")) {
                final String[] split = line.split(":");
                optimum = Long.valueOf(split[1].trim());
                break;
            }
        }
    }

    private static void readCoordinates(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        boolean reachedCoordinates = false;
        while (scanner.hasNextLine()) {
            if (!reachedCoordinates && !"NODE_COORD_SECTION".equals(scanner.nextLine())) continue;
            reachedCoordinates = true;
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            String id;
            if (lineScanner.hasNext() && !(id = lineScanner.next()).equals("EOF")) {
                float y = Float.parseFloat(lineScanner.next());
                float x = Float.parseFloat(lineScanner.next());
                vertices.add(new Vertex(Integer.parseInt(id), x, y));
            }
        }
    }

    private static void addVertices() {
        calculateMinXAndMinY();
        vertices.forEach(v -> {
            v.getGraphic().setCenterX(v.getGraphic().getCenterX() - minX);
            v.getGraphic().setCenterY(v.getGraphic().getCenterY() - minY);
            contentPane.getChildren().add(v.getGraphic());
        });
        //without it, the zoompane size is in some cases too small
//        zoomPane.autosize();
    }


    private static void calculateMinMaxRangeRatio() {
        calculateMinXAndMinY();
        contentPane.requestLayout();
        final double rootScrollPaneWidth = rootScrollPane.getWidth();
        final double contentPaneWidth = contentPane.getBoundsInParent().getWidth();
        xRatio = rootScrollPaneWidth / contentPaneWidth;
        final double rootScrollPaneHeight = rootScrollPane.getHeight();
        final double contentPaneHeight = contentPane.getBoundsInParent().getHeight();
        yRatio = rootScrollPaneHeight / contentPaneHeight;
    }

    private static void rotateContentPane() {
        contentPane.setLayoutX((-contentPane.getLayoutBounds().getWidth() / 2) - contentPane.getLayoutBounds().getMinX());
        contentPane.setLayoutX((-contentPane.getLayoutBounds().getHeight() / 2) - contentPane.getLayoutBounds().getMinY());
        contentPane.setRotate(180);
    }

    private static void calculateMinXAndMinY() {
        Optional<Vertex> optMinX = vertices.stream().min(Comparator.comparing(Vertex::getX));
        minX = optMinX.map(Vertex::getX)
                .orElse(0.0);

        Optional<Vertex> optMinY = vertices.stream().min(Comparator.comparing(Vertex::getY));
        minY = optMinY.map(Vertex::getY)
                .orElse(0.0);
    }


    private static void removePreviousScaling() {
        //remove previous scaling
        contentPane.setScaleX(1);
        contentPane.setScaleY(1);
    }

    private static void scaleContentPane() {
        double ratio = Math.min(xRatio, yRatio); //
        ratio *= 0.98; //otherwise some vertices at boundary doesnt fit
        contentPane.setScaleX(ratio);
        contentPane.setScaleY(ratio);
        compensateRatioOfVertices(ratio);

        contentPane.setTranslateX(contentPane.getTranslateX() - contentPane.getBoundsInParent().getMinX());
        contentPane.setTranslateY(contentPane.getTranslateY() - contentPane.getBoundsInParent().getMinY());
    }

    private static void compensateRatioOfVertices(double ratio) {
        contentPane.getChildren()
                .stream()
                .filter(Circle.class::isInstance)
                .forEach(node -> {
                    node.setScaleX(1 / ratio);
                    node.setScaleY(1 / ratio);
                });
    }

    static void clearContentPane() {
        contentPane.getChildren().clear();
        removePreviousScaling();
    }

    private static void clipChildren(Region region) {
        final Rectangle outputClip = new Rectangle(region.getWidth(), region.getHeight());
        // bind properties so height and width of rect changes according pane's width and height
        outputClip.heightProperty().bind(region.heightProperty());
        outputClip.widthProperty().bind(region.widthProperty());
        region.setClip(outputClip);
    }

    public synchronized static void drawEdge(Edge edge) {
        final double scale = contentPane.getScaleX();
        final Line line = edge.getGraphic();
        line.setStrokeWidth(line.getStrokeWidth() / scale);
        contentPane.getChildren().remove(line);
        contentPane.getChildren().add(line);
        edges.add(edge);
    }

    public static void removeAllEdges() {
        if(null != contentPane && 0 < contentPane.getChildren().size()) {
            edges.clear();
            contentPane.getChildren().removeIf(Line.class::isInstance);
        }
    }

    static void removeAllVertices() {
        if(null != contentPane && 0 < contentPane.getChildren().size()) {
            contentPane.getChildren().removeIf(Circle.class::isInstance);
        }
    }

    public static void deleteEdge(Edge e) {
        edges.remove(e);
        contentPane.getChildren()
                .removeIf(line -> line.equals(e.getGraphic()));
    }

    static List<Edge> insertTour(List<Integer> verticesIds) {
        List<Edge> tour = new ArrayList<>();
        if (verticesIds.size() == vertices.size()) {
            for (int i = 0; i < verticesIds.size() - 1; i++) {
                Optional<Vertex> from = getVertex(verticesIds.get(i));
                Optional<Vertex> to = getVertex(verticesIds.get(i + 1));
                if (!from.isPresent() || !to.isPresent()) {
                    logger.warning("Tour Edge with following Vertex ids can't be drawn because they are not available: " + verticesIds.get(i) + ", " + verticesIds.get(i + 1));
                    return new ArrayList<>();
                }
                final Edge edge = new Edge(from.get(), to.get());
                Platform.runLater(() -> drawEdge(edge));
//                costs = costs.add(AbstractRouteCalculator.cost(from.get(), to.get()));
                tour.add(edge);
            }
            Optional<Vertex> from = getVertex(verticesIds.get(verticesIds.size() - 1));
            Optional<Vertex> to = getVertex(verticesIds.get(0));
            if (from.isPresent() && to.isPresent()) {
                final Edge edge = new Edge(from.get(), to.get());
                Platform.runLater(() -> drawEdge(edge));
//                costs = costs.add(AbstractRouteCalculator.cost(from.get(), to.get()));
                tour.add(edge);
            }
        }
        return tour;
    }

    private static Optional<Vertex> getVertex(int id) {
        return vertices.stream().filter(vertex -> vertex.getId() == id).findFirst();
    }

    public static int getMinNode() {
        final Optional<Map.Entry<Vertex, Double>> minVertex = vertices.stream()
                .collect(Collectors.toMap(v -> v, v -> v.getX() + v.getY()))
                .entrySet()
                .stream().min(Comparator.comparing(Map.Entry::getValue));
        return minVertex
                .map(vertexDoubleEntry -> vertices.indexOf(vertexDoubleEntry.getKey()))
                .orElse(0);
    }

    public static int getMaxNode() {
        final Optional<Map.Entry<Vertex, Double>> maxVertex = vertices.stream()
                .collect(Collectors.toMap(v -> v, v -> v.getX() + v.getY()))
                .entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue));
        return maxVertex
                .map(vertexDoubleEntry -> vertices.indexOf(vertexDoubleEntry.getKey()))
                .orElse(vertices.size() - 1);
    }

    public static void setRandomNode() {
        Random random = new Random();
        randomNode = random.nextInt(vertices.size());
    }

    public static int getRandomNode() {
        return randomNode;
    }

    public static int getAverageNode() {
        DoubleSummaryStatistics stats = vertices.stream()
                .mapToDouble(v-> v.getX()+v.getY())
                .summaryStatistics();
        double average = stats.getAverage();
        final Optional<Map.Entry<Vertex, Double>> averageVertex = vertices.stream()
                .collect(Collectors.toMap(v -> v, v -> v.getX() + v.getY()))
                .entrySet().stream()
                .min(Comparator.comparingDouble(e -> Math.abs(e.getValue() - average)));

        return averageVertex
                .map(vertexDoubleEntry -> vertices.indexOf(vertexDoubleEntry.getKey()))
                .orElse(getRandomNode());
    }

    public static void redrawVertices() {
        removeAllVertices();
        vertices.forEach(v -> contentPane.getChildren().add(v.getGraphic()));
    }

    public static void redrawEdges() {
        final List<Edge> backupEdges = new ArrayList<>(edges);
        removeAllEdges();
        backupEdges.forEach(GraphDrawer::drawEdge);
    }

    public synchronized static void redrawEdges(List<Edge> bestTour) {
        removeAllEdges();
        bestTour.forEach(GraphDrawer::drawEdge);
    }
}

