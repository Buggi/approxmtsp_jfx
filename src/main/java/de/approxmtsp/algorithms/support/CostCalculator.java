package de.approxmtsp.algorithms.support;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;

import java.util.List;

public class CostCalculator {

    public static double cost(Vertex src, Vertex dest) {
        return euclideanDistance(src, dest);
    }

    public static double cost(Edge edge) {
        if (null == edge) {
            return Double.POSITIVE_INFINITY;
        }
        return euclideanDistance(edge.getSource(), edge.getTarget());
    }

    private static double euclideanDistance(Vertex v1, Vertex v2) {
        double xPowed = Math.pow(v2.getX() - (v1.getX()), 2);
        double yPowed = Math.pow(v2.getY() - (v1.getY()), 2);

        return Math.sqrt(xPowed + yPowed);
    }

    public static double spanningTreeCost(List<Edge> mst) {
        double sum = 0.0;
        if (null == mst) {
            Platform.runLater(() -> AlertBox.display("mst is null", "mst is null"));
            return Double.POSITIVE_INFINITY;
        }
        for (Edge edge : mst) {
            double cost = cost(edge);
            sum += cost;
        }
        return sum;
    }

    public static double tourCosts(List<Edge> tour) {
        if (tour.isEmpty()) {
            return Double.POSITIVE_INFINITY;
        }
        return tour.stream()
                .mapToDouble(CostCalculator::cost)
                .sum();
    }

    public static int tourCostsRounded(List<Edge> tour) {
        return tour.stream()
//                .mapToInt(edge -> Math.round(euclideanDistance(edge.getSource(), edge.getTarget())))
                .mapToInt(edge -> (int)Math.round(euclideanDistance(edge.getSource(), edge.getTarget())))
                .sum();
    }
}
