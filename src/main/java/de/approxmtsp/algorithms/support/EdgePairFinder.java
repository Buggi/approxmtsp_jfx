package de.approxmtsp.algorithms.support;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.EdgePair;
import de.approxmtsp.graph.Vertex;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static de.approxmtsp.algorithms.support.CostCalculator.cost;
import static de.approxmtsp.algorithms.support.FeasibilityChecker.isUsed;

public class EdgePairFinder {
    private final List<Vertex> vertices;
    private final List<Edge> allPossibleEdges;
    private final Map<String, Double> zMap;

    public EdgePairFinder(List<Vertex> vertices, List<Edge> allPossibleEdges, Map<String, Double> zMap) {
        this.vertices = vertices;
        this.allPossibleEdges = allPossibleEdges;
        this.zMap = zMap;
    }

    /**
     * @param possibleXEdges possible x edges: 2 possible xi for i > 1; else -> any tour edge
     * @param triedPairs pairs that are already used
     * @param t1i null when x1-y1 is wanted
     * @param tour current tour
     * @return  most out of placed pair
     *          OR null when all possible pairs are tried already
     */
    public EdgePair findMostOutOfPlacePair(List<Edge> possibleXEdges, List<EdgePair> triedPairs, Vertex t1i, List<Edge> tour) {
        double bestX1Y1Gain = Double.NEGATIVE_INFINITY;
        Edge x = null, y = null;
        for (Edge edge : possibleXEdges) {
            Edge tempY1 = getY(edge, tour, triedPairs, t1i);
            double x1Y1Gain = calculateGain(edge, tempY1);
            if (bestX1Y1Gain < x1Y1Gain) {
                bestX1Y1Gain = x1Y1Gain;
                x = edge;
                y = tempY1;
            }
        }
        if (null == x || null == y)
            return null;
        return new EdgePair(x, y);
    }

    private boolean isPairUsedBefore(Edge tempX1, Edge tempY1, List<EdgePair> triedPairs) {
        final Edge finalTempX = tempX1;
        final Edge finalTempY = tempY1;
        return triedPairs.stream()//
                .filter(Objects::nonNull)
                .filter(pair -> finalTempX.equals(pair.getX()))//
                .anyMatch(pair -> finalTempY.equals(pair.getY()));
    }

//  private Edge getX1(Vertex t1, List<Edge> tempTour) throws RouteCalculatorException {
//    Optional<Edge> x1Opt = tempTour.stream()
//      .filter(edge -> (edge.getSource().getId() == t1.getId()) || edge.getTarget().getId() == t1.getId())
//      //TODO: take edge with max costs / gain
//      .findFirst();
//    if(!x1Opt.isPresent()) {
//      Platform.runLater(() -> AlertBox.display("Lin-Kernighan error", "Could not determine x1"));
//      throw new RouteCalculatorException("Lin-Kernighan error: Could not determine x1");
//    }
//    return x1Opt.get();
//  }

    private Edge getY(Edge x, List<Edge> tempTour, List<EdgePair> triedPairs, Vertex t1i) {
        return allPossibleEdges.stream()
                .filter(e -> !e.equals(x))//
                .filter(e -> !isPairUsedBefore(x, e, triedPairs))//
                .filter(e -> isNeighbour(x, e, t1i))//
                .filter(e -> !isUsed(tempTour, e))//
                .min(Comparator.comparingDouble(e -> zMap.get(e.getId())))//
                .orElse(null);
    }

    private boolean isNeighbour(Edge edge, Edge possibleNeighbour, Vertex t1i) {
        final Vertex v1 = edge.getSource();
        final Vertex v2 = edge.getTarget();
        final Vertex v1Neighbour = possibleNeighbour.getSource();
        final Vertex v2Neighbor = possibleNeighbour.getTarget();
        //if y1 is wanted
        if (null == t1i) {
            return v1.equals(v1Neighbour) || //
                    v1.equals(v2Neighbor) || //
                    v2.equals(v1Neighbour) || //
                    v2.equals(v2Neighbor);
        }
        //if any y is wanted sequential
//        final Vertex t1 = v1.equals(t1i) ? v1 : v2;
        final Vertex t2 = v1.equals(t1i) ? v2 : v1;
        return t2.equals(v1Neighbour) || t2.equals(v2Neighbor);
    }

    private static double calculateGain(Edge x, Edge y) {
        if (null == x || null == y) {
            return Double.NEGATIVE_INFINITY;
        }
        double costX = cost(x.getSource(), x.getTarget());
        double costY = cost(y.getSource(), y.getTarget());
        return costX - costY;
    }
}
