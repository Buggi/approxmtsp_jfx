package de.approxmtsp.algorithms.support;

import static de.approxmtsp.algorithms.tourcalculator.AbstractRouteCalculator.cost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;

public class MSTCalculator {

    private static de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap heap = new de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap();

    //see ChristofidesCalculator
    public static Map<Vertex, Vertex> createParentMap(Vertex startVertex, List<Vertex> vertices) throws InterruptedException {
        heap.clear();
        final List<Vertex> tempVertices = new ArrayList<>(vertices);
        tempVertices.remove(startVertex);
        Map<FibonacciHeap.Node, Double> costMap = new HashMap<>();
        Map<Vertex, Vertex> parentMap = new LinkedHashMap<>();
        //        TreeMap<Vertex,Vertex> parentMap = new TreeMap<>();
        heap.insert(startVertex, 0);
        parentMap.put(startVertex, null);
        for (Vertex vertex : tempVertices) {
            FibonacciHeap.Node node = heap.insert(vertex, cost(startVertex, vertex));
            costMap.put(node, Double.POSITIVE_INFINITY);
            parentMap.put(vertex, null);
        }
        FibonacciHeap.Node minNode = null;
        while (0 != heap.size()) {
            final Vertex lastMinVertex = null != minNode ? (Vertex) minNode.getObject() : null;
            minNode = heap.removeMin();
            costMap.remove(minNode);
            for (Map.Entry<FibonacciHeap.Node, Double> entry : costMap.entrySet()) {
                final double costs = cost((Vertex) minNode.getObject(), (Vertex) entry.getKey().getObject());
                if (costs < entry.getValue()) {
                    entry.setValue(costs);
                    heap.decreaseKey(entry.getKey(), costs);
                    parentMap.put((Vertex) entry.getKey().getObject(), (Vertex) minNode.getObject());
                }
            }
        }
        //        synchronized (observableEdges) {
        //            if (synchronizedPauseOrStop()) return null;
        //            parentMap.forEach((key, value) -> {
        //                if (null != key && null != value) {
        //                    final Edge edge = new Edge(key, value);
        //                    drawLater(edge);
        //                    observableEdges.add(edge);
        //                }
        //            });
        //
        //        }
        //        observableEdges.clear();
        //        GraphDrawer.removeAllEdges();
        return parentMap;
    }

    public static List<Edge> createMst(Map<Vertex, Vertex> parentMap) {
        return parentMap.entrySet().stream()
                .filter(Objects::nonNull)
                .filter(entry -> entry.getKey() != null)
                .filter(entry -> entry.getValue() != null)
                .map(entry -> new Edge(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }


}
