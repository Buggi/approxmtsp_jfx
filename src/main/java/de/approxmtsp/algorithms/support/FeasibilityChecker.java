package de.approxmtsp.algorithms.support;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class FeasibilityChecker {
    private FeasibilityChecker() {
    }

    public static boolean isTourFeasible(List<Edge> tour, List<Vertex> vertices) {
        if (hasDegreeTwoViolation(tour, vertices) ||
                tour.size() != vertices.size())
            return false;

        Edge start = tour.get(0);
        Optional<Edge> currEdge = findNextEdge(tour, start, null);

        Edge prev = start;
        if (!currEdge.isPresent()) {
            return false;
        }
        int size = 2;
        while (size < tour.size()) {
            if (start.equals(currEdge.get())) {
                return false;
            }
            Optional<Edge> nextEdge = findNextEdge(tour, currEdge.get(), prev);
            if (!nextEdge.isPresent()) {
                return false;
            }
            size++;
            prev = currEdge.get();
            currEdge = nextEdge;
        }
        // check if last/currEdge is connected to start edge
        return currEdge.get().getSource().equals(start.getSource()) ||
                currEdge.get().getSource().equals(start.getTarget()) ||
                currEdge.get().getTarget().equals(start.getTarget()) || currEdge.get().getTarget().equals(start.getSource());
    }

    private static boolean hasDegreeTwoViolation(List<Edge> tour, List<Vertex> vertices) {
        Map<Integer, Long> countSources = tour.stream().map(Edge::getSource)
                .collect(Collectors.groupingBy(Vertex::getId, Collectors.counting()));
        Map<Integer, Long> countTargets = tour.stream().map(Edge::getTarget)
                .collect(Collectors.groupingBy(Vertex::getId, Collectors.counting()));

        countSources.forEach((key, value) -> countTargets.merge(key, value, Long::sum));
        if (vertices.size() != countTargets.size())
            return true;
        Optional<Vertex> invalidVertex = vertices.stream().filter(Objects::nonNull)
                .filter(v -> countTargets.get(v.getId()) != 2).findFirst();
        if (invalidVertex.isPresent())
            return true;
        return false;
    }

    private static Optional<Edge> findNextEdge(List<Edge> tour, Edge curr, Edge previous) {
        int currSrcId = curr.getSource().getId();
        int currTgtId = curr.getTarget().getId();
        return tour.stream()//
                .filter(edge -> edge.getSource().getId() == currSrcId || edge.getSource().getId() == currTgtId ||
                        edge.getTarget().getId() == currSrcId || edge.getTarget().getId() == currTgtId)
                .filter(edge -> !edge.equals(previous))//
                .filter(edge -> !edge.equals(curr))//
                .findFirst();
    }

    public static boolean isUsed(List<Edge> edges, Edge edge) {
        return isUsed(edges, edge.getSource(), edge.getTarget());
    }
    private static boolean isUsed(List<Edge> edges, Vertex v1, Vertex v2) {
        Optional<Edge> optEdge = edges.stream()//
                .filter(Objects::nonNull)
                .filter(
                        edge -> (edge.getSource().getId() == v1.getId() && edge.getTarget().getId() == v2.getId()) ||
                                (edge.getSource().getId() == v2.getId() && edge.getTarget().getId() == v1.getId()))//
                .findFirst();

        return optEdge.isPresent();
    }
}
