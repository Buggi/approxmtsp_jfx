package de.approxmtsp.algorithms.support;

import de.approxmtsp.algorithms.tourcalculator.AbstractRouteCalculator;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.approxmtsp.algorithms.support.CostCalculator.spanningTreeCost;
import static de.approxmtsp.algorithms.support.MSTCalculator.createMst;
import static de.approxmtsp.algorithms.support.MSTCalculator.createParentMap;
import static java.util.Collections.singletonList;

public class ZMapCalculator {

  private final Map<Edge, Double> costMap;
  private final List<Edge> allPossibleEdges;
  private final List<Vertex> vertices;

  public ZMapCalculator(List<Edge> allPossibleEdges, List<Vertex> vertices) {
    costMap = createCostMap(allPossibleEdges);
    this.allPossibleEdges = allPossibleEdges;
    this.vertices = vertices;
  }

  public Map<String, Double> createZMap() throws InterruptedException {
    Map<Vertex, Vertex> parentMap = createParentMap(vertices.get(0), vertices);
    List<Edge> mst = createMst(parentMap);
    Map<String, Double> inMap = new HashMap<>();
    Map<String, Double> exMap = new HashMap<>();

    allPossibleEdges.parallelStream()//
      .forEach(edge -> createInAndExEntry(mst, inMap, exMap, edge));
        /* Finden eines MST mit der Kante dauert zu lange
        List<List<Edge>> msts = new ArrayList<>();
        msts.add(createMst(createInParentMap(allPossibleEdges.get(0))));
        Map<Edge, List<Edge>> inMap = new HashMap<>();
        Map<Edge, List<Edge>> exMap = new HashMap<>();



        for (Edge edge : allPossibleEdges) {
            final Optional<List<Edge>> optInMst = msts.stream().parallel()
                    .filter(mst -> mst.contains(edge))
                    .findFirst();
            final List<Edge> inMst = optInMst.orElseGet(() -> createMst(createInParentMap(edge)));
            inMap.put(edge, inMst);
            if (!optInMst.isPresent())
                msts.add(inMst);

            final Optional<List<Edge>> optExMst = msts.stream().parallel()
                    .filter(mst -> !mst.contains(edge))
                    .findFirst();
            final List<Edge> exMst = optExMst.orElseGet(() -> createMst(createExParentMap(edge)));
            exMap.put(edge, exMst);
            if (!optExMst.isPresent())
                msts.add(exMst);
        }
*/
        /*
        // Für alle möglichen Kanten:
        // IN = Berechne MST, welcher diese Kante enthält
        // EX = Berechne MST, welcher diese Kante NICHT enthält
        // Z = cost(IN) - cost(EX)
        // Map für MST welche die Kante (key) enthalten
        Map<Edge, List<Edge>> inMap = allPossibleEdges.parallelStream()
                .collect(Collectors.toMap(e -> e, e -> createMst(createInParentMap(e))));
        // Map für MST welche die Kante (key) nicht enthalten
        Map<Edge, List<Edge>> exMap = allPossibleEdges.parallelStream()
                .collect(Collectors.toMap(e -> e, e -> createMst(createExParentMap(e))));

         */
    // Z Map: Kosten IN - Kosten EX
    // --> falls Kante im MST: Z <= 0
    // --> falls Kante nicht im MST Z > 0

    return allPossibleEdges.stream()/*.parallelStream()*/
            .collect(Collectors.toMap(Edge::getId, e -> inMap.get(e.getId()) - exMap.get(e.getId())));
  }

  private Map<Edge, Double> createCostMap(List<Edge> allPossibleEdges) {
    return allPossibleEdges.parallelStream()
            .filter(e -> !e.getTarget().equals(e.getSource()))
            .collect(Collectors.toMap(edge -> edge, AbstractRouteCalculator::cost, (a, b) -> b));
  }

  private void createInAndExEntry(List<Edge> mst, Map<String, Double> inMap, Map<String, Double> exMap, Edge edge) {
    List<Edge> tempMst = new ArrayList<>(mst);
    boolean isIn = mst.contains(edge);
    if(isIn) {
      addToMap(mst, inMap, edge);
      final List<Edge> exMst = createExMst(edge, tempMst);
      addToMap(exMst, exMap, edge);
    }
    else {
      addToMap(mst, exMap, edge);
      //now mst contains exactly one circle
      tempMst.add(edge);
      //find circle in the mst
      final List<Edge> circle = findCircleInMst(edge, tempMst);
      //delete the most expensive edge of the circle (not the added edge of course)
      final Edge maxEdge = circle.stream().filter(e -> !e.equals(edge))
        .max(Comparator.comparingDouble(AbstractRouteCalculator::cost)).orElse(null);
      tempMst.remove(maxEdge);
      addToMap(tempMst, inMap, edge);
    }
  }

  private synchronized void addToMap(List<Edge> mst, Map<String, Double> map, Edge edge) {
    if(null == map) {
      Platform
        .runLater(() -> AlertBox
          .display("map is null", "Somehow map orExmap for edge " + edge.getId() + " is null"));
      return;
    }
    final double cost = spanningTreeCost(mst);
    map.put(edge.getId(), cost);
  }

  private List<Edge> createExMst(Edge edge, List<Edge> tempMst) {
    final Vertex leaf = getLeaf(edge, tempMst);
    //1.remove edge from mst
    tempMst.remove(edge);
    //if v edge is a leaf
    if(null != leaf) {
      //                    -connect the leaf vertex with the cheapest vertex (edge must not exist)
      final Optional<Map.Entry<Edge, Double>> minEdge = costMap.entrySet().stream()
        .filter(e -> e.getKey().getSource().equals(leaf) || e.getKey().getTarget().equals(leaf))
        .filter(e -> !tempMst.contains(e.getKey())).min(Comparator.comparingDouble(Map.Entry::getValue));
      if(!minEdge.isPresent()) {
        Platform.runLater(() -> AlertBox
          .display("Could not find any edge which contains leaf", "Could not find any edge which contains leaf"));
        return null;
      }
      final Edge minEdgeKey = minEdge.get().getKey();
      if(null == minEdgeKey) {
        Platform.runLater(() -> AlertBox
          .display("Could not find any edge which contains leaf", "Could not find any edge which contains leaf"));
        return null;
      }
      tempMst.add(minEdgeKey);
    }
    else {
      //                  -find every vertex from one subtree
      final Vertex sourceVertex = edge.getSource();
      final List<Vertex> subtree = new ArrayList<>();
      List<Vertex> verticesConnectedToCurrEdge = new ArrayList<>();
      verticesConnectedToCurrEdge.add(sourceVertex);
      do {
        final List<Vertex> finalVerticesConnectedToCurrEdge = verticesConnectedToCurrEdge;
        verticesConnectedToCurrEdge = tempMst.stream().filter(
          e -> finalVerticesConnectedToCurrEdge.contains(e.getSource()) ||
               finalVerticesConnectedToCurrEdge.contains(e.getTarget()))
          .map(e -> getOtherVertexOfEdge(e, finalVerticesConnectedToCurrEdge)).filter(Objects::nonNull)
          .filter(vertex -> !subtree.contains(vertex)).collect(Collectors.toList());
        subtree.addAll(verticesConnectedToCurrEdge);
      }
      while(!verticesConnectedToCurrEdge.isEmpty());
      //                  -find for every vertex the cheapest connection/edge to the other subtree
      final Set<Vertex> otherSubtree = tempMst.stream().flatMap(e -> Stream.of(e.getSource(), e.getTarget()))
        .filter(v -> !subtree.contains(v)).collect(Collectors.toSet());

      double minCosts = Double.POSITIVE_INFINITY;
      Edge minEdge = null;
      for(Vertex vertex : subtree) {
        for(Vertex vertexOther : otherSubtree) {
          double cost = CostCalculator.cost(vertex, vertexOther);
          //                  -determine the cheapest edge
          if(cost < minCosts) {
            minCosts = cost;
            minEdge = new Edge(vertex, vertexOther);
          }
        }
      }
      //                  -add this edge to the spanning tree
      tempMst.add(minEdge);
    }
    return tempMst;
  }

  //TODO: still inefficient: maybe try https://www.geeksforgeeks.org/detect-cycle-undirected-graph/
  private List<Edge> findCircleInMst(Edge edge, List<Edge> mst) {
    List<Edge> tempMst = new ArrayList<>(mst);
    final Vertex startVertex = edge.getSource();
    final List<Edge> circle = new ArrayList<>();
    final Vertex nextVertex = getOtherVertexOfEdge(edge, singletonList(startVertex));
    return findCircle(tempMst, startVertex, nextVertex, edge, circle);
  }

  private List<Edge> findCircle(List<Edge> tempMst, Vertex startVertex, Vertex currVertex, Edge currEdge,
                                List<Edge> tempCircle) {
    tempCircle.add(currEdge);
    tempMst.remove(currEdge);
    if(startVertex.equals(currVertex)) {
      return tempCircle;
    }
    final List<Edge> nextEdges = findNextEdges(tempMst, currVertex);
    for(Edge ne : nextEdges) {
      final Vertex nv = getOtherVertexOfEdge(ne, singletonList(currVertex));

      final List<Edge> c = findCircle(tempMst, startVertex, nv, ne, tempCircle);
      if(null != c) {
        return c;
      }
    }

    return null;
  }

  private List<Edge> findNextEdges(List<Edge> mst, Vertex lastVertex) {
    return mst.parallelStream()
      .filter(edge -> edge.getSource().getId() == lastVertex.getId() || edge.getTarget().getId() == lastVertex.getId())//
      .collect(Collectors.toList());
  }

  private Vertex getOtherVertexOfEdge(Edge e, List<Vertex> currVertices) {
    if(currVertices.contains(e.getSource()))
      return e.getTarget();
    else if(currVertices.contains(e.getTarget()))
      return e.getSource();
    return null;
  }

  private Vertex getLeaf(Edge edge, List<Edge> mst) {
    return mst.parallelStream()
            .filter(e -> e.equals(edge))
            .flatMap(e -> Stream.of(e.getSource(), e.getTarget()))
            .filter(v -> isVertexLeaf(v, mst))
            .findFirst().orElse(null);
  }

  private boolean isVertexLeaf(Vertex vertex, List<Edge> mst) {
    final long count = mst.stream().filter(edge -> edge.getSource().equals(vertex) || edge.getTarget().equals(vertex))
      .count();
    return 1 == count;
  }

}
