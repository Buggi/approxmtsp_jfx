package de.approxmtsp.algorithms.heapimplementations.miller;

import de.approxmtsp.graph.Vertex;

import java.util.Arrays;
import java.util.PriorityQueue;
//https://courses.cs.washington.edu/courses/cse373/11wi/homework/5/BinaryHeap.java


public class BinaryHeap<T extends Comparable<T>> extends PriorityQueue<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] array;
    private int size;

    /**
     * Constructs a new BinaryHeap.
     */
     BinaryHeap() {
        // Java doesn't allow construction of arrays of placeholder data types
        array = (T[]) new Comparable[DEFAULT_CAPACITY];
        size = 0;
    }


    /**
     * Adds a value to the min-heap.
     */
    public boolean add(T value) {
        if (null == value) {
            throw new NullPointerException("value is null");
        }
        // grow array if needed
        if (size >= array.length - 1) {
            array = this.resize();
        }

        // place element into heap at bottom
        size++;
        int index = size;
        array[index] = value;

        bubbleUp(this.size);
        return true;
    }


    /**
     * Returns true if the heap has no elements; false otherwise.
     */
    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * Returns (but does not remove) the minimum element in the heap.
     */
    public T peek() {
        if (this.isEmpty()) {
            throw new IllegalStateException();
        }

        return array[1];
    }


    /**
     * Removes and returns the minimum element in the heap.
     */
    public T remove() {
        // what do want return?
        T result = peek();

        // get rid of the last leaf/decrement
        array[1] = array[size];
        array[size] = null;
        size--;

        bubbleDown();

        return result;
    }


    /**
     * Returns a String representation of BinaryHeap with values stored with
     * heap structure and order properties.
     */
    public String toString() {
        return Arrays.toString(array);
    }


    /**
     * Performs the "bubble down" operation to place the element that is at the
     * root of the heap in its correct place so that the heap maintains the
     * min-heap order property.
     */
    private void bubbleDown() {
        int index = 1;

        // bubble down
        while (hasLeftChild(index)) {
            // which of my children is smaller?
            int smallerChild = leftIndex(index);

            // bubble with the smaller child, if I have a smaller child
            if (hasRightChild(index)
                    && array[leftIndex(index)].compareTo(array[rightIndex(index)]) > 0) {
                smallerChild = rightIndex(index);
            }

            if (array[index].compareTo(array[smallerChild]) > 0) {
                swap(index, smallerChild);
            } else {
                // otherwise, get outta here!
                break;
            }

            // make sure to update loop counter/index of where last el is put
            index = smallerChild;
        }
    }


    /**
     * Performs the "bubble up" operation to place a newly inserted element
     * (i.e. the element that is at the size index) in its correct place so
     * that the heap maintains the min-heap order property.
     */

//    protected void bubbleUp() {
//        int index = this.size;
//
//        while (hasParent(index)
//                && (parent(index).compareTo(array[index]) > 0)) {
//            // parent/child are out of order; swap them
//            swap(index, parentIndex(index));
//            index = parentIndex(index);
//        }
//    }

    private void bubbleUp(int index) {
        while (hasParent(index)
                && (parent(index).compareTo(array[index]) > 0)) {
            // parent/child are out of order; swap them
            swap(index, parentIndex(index));
            index = parentIndex(index);
        }
    }


    private boolean hasParent(int i) {
        return i > 1;
    }


    private int leftIndex(int i) {
        return i * 2;
    }


    private int rightIndex(int i) {
        return i * 2 + 1;
    }


    private boolean hasLeftChild(int i) {
        return leftIndex(i) <= size;
    }


    private boolean hasRightChild(int i) {
        return rightIndex(i) <= size;
    }


    private T parent(int i) {
        return array[parentIndex(i)];
    }


    private int parentIndex(int i) {
        return i / 2;
    }


    private T[] resize() {
        return Arrays.copyOf(array, array.length * 2);
    }


    private void swap(int index1, int index2) {
        T tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;
    }

    void decreaseKey(Vertex vertex, Double costs) {
        int index = -1;
        for (int i = 1; i <= size; i++) {
            if (array[i].equals(vertex)) {
                //TODO:
//                ((Vertex)array[i]).setCosts(costs);
                index = i;
                break;
            }
        }
        bubbleUp(index);
    }
}

////https://stackoverflow.com/questions/27586437/building-a-min-heap-using-java answered Dec 14 '17 at 14:28 user3060544
//public class VertexHeap {
//    private Vertex[] heap;
//    private int size;
//    private static final int FRONT = 1;
//    private Vertex startVertex;
//
//    public VertexHeap(int maxSize, Vertex startVertex) {
//        heap = new Vertex[maxSize + 1];
//        size = 0;
//        this.startVertex = startVertex;
//    }
//
//    public void insert(Vertex vertexInsert) {
//        vertexInsert.setCosts(cost(vertexInsert, startVertex));
//        heap[++size] = vertexInsert;
//        int index = size;
//        while (hasParent(index) && heap[index].getCosts() < heap[getParent(index)].getCosts()) {
//            swap(index, getParent(index));
//            index = getParent(index);
//        }
//    }
//
//    //extractMin?
//    public Vertex delete() {
//        Vertex item = heap[FRONT];
//        swap(FRONT, size--); // heap[FRONT] = heap[size--];
//        heapify(FRONT);
//        return item;
//    }
//
//    private double cost(Vertex src, Vertex dest) {
//        //euclidean distance
//        final double sum = squared((dest.getX() - src.getX())) + squared((dest.getY() - src.getY()));
//        return Math.sqrt(sum);
//    }
//
//    private double squared(double a) {
//        return Math.pow(a, 2);
//    }
//
//    private boolean hasParent(int position) {
//        return 1 < position;
//    }
//
//    private int getParent(int position) {
//        return position / 2;
//    }
//
//    private int getLeftChild(int position) {
//        return position * 2;
//    }
//
//    private int getRightChild(int position) {
//        return position * 2 + 1;
//    }
//
//    private void swap(int position1, int position2) {
//        Vertex temp = heap[position1];
//        heap[position1] = heap[position2];
//        heap[position2] = temp;
//    }
//
//    private boolean isLeaf(int position) {
//        if (position > size / 2) {
//            return true;
//        }
//        return false;
//    }
//
//    private void heapify(int position) {
//        if (isLeaf(position)) {
//            return;
//        }
//        while(hasLef)
//        if (heap[position].getCosts() > heap[getLeftChild(position)].getCosts() || heap[position].getCosts() > heap[getRightChild(position)].getCosts()) {
//            // if left is smaller than right
//            if (heap[getLeftChild(position)].getCosts() < heap[getRightChild(position)].getCosts()) {
//                // swap with left
//                swap(position, getLeftChild(position));
//                heapify(getLeftChild(position));
//            } else {
//                // swap with right
//                swap(position, getRightChild(position));
//                heapify(getRightChild(position));
//            }
//        }
//    }
//
//    public Vertex extractMin() {
//        Vertex item = heap[FRONT];
//        swap(FRONT, size--); // heap[FRONT] = heap[size--];
//        heapify(FRONT);
//        return item;
////        if (0 < heap.length) {
////            return heap[1];
////        }
////        throw new IllegalStateException("VertexHeap.extractMin(): Vertex Heap contains no elements");
//    }
//
//    public void decreaseKey(Vertex vertex, Double costs) {
//        int index = -1;
//        for (int i = 1; i < heap.length; i++) {
//            if (heap[i].equals(vertex)) {
//                heap[i].setCosts(costs);
//                index = i;
//                break;
//            }
//        }
//        while (hasParent(index) && heap[index].getCosts() < heap[getParent(index)].getCosts()) {
//            swap(index, getParent(index));
//            index = getParent(index);
//        }
//    }
//}
