package de.approxmtsp.algorithms;

public class RouteCalculatorException extends RuntimeException {
    public RouteCalculatorException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
