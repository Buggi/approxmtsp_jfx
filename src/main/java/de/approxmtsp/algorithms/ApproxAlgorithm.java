package de.approxmtsp.algorithms;

public enum ApproxAlgorithm {
    NEAREST_NEIGHBOUR (),
    DOUBLE_ENDED_NEAREST_NEIGHBOUR(),
    NEAREST_INSERTION (),
    FARTHEST_INSERTION (),
    FARTHEST_INSERTION_LOWEST_DISTANCE_MAX(),
    CHEAPEST_INSERTION (),
    SPANNING_TREE (),
    CHRISTOFIDES (),
    RANDOM();

    ApproxAlgorithm() {
    }

}
