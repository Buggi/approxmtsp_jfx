package de.approxmtsp.algorithms.listimplementations;
/*
 *   sources:
 *   https://www.geeksforgeeks.org/doubly-linked-list/
 */


import de.approxmtsp.ui.AlertBox;
import org.apache.commons.collections4.ListUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

//Doubly Circular Linked List
public class DoublyLinkedList<E> {
    private Node head;
    private Node tail;
    private int size;

    public int size() {
        return size;
    }

    class Node {

        public E data;

        Node prev;

        Node next;
        Node(E data) {
            this.data = data;
        }

    }
    public boolean isEmpty() {
        return head == null;
    }
    public void push(E data) {
        Node node = new Node(data);
        node.next = head;
        if (null != head)
            head.prev = node;
        if (null == tail)
            tail = node;
        node.prev = tail;
        head = node;
        size++;
    }

    public void append(E data) {
        Node newNode = new Node(data);
        newNode.prev = tail;
        if (null != tail)
            tail.next = newNode;
        tail = newNode;
        newNode.next = head;
        if (null == head) {
            newNode.prev = tail;
            newNode.next = tail;
            head = newNode;
            size++;
            return;
        }
        head.prev = tail;
        size++;
    }

    public void insertAfter(Node prev, E data) {
        if (null == prev) {
            AlertBox.display("Previous Node is null", "Previous Node in Doubly Linked List cannot be NULL!");
            return;
        }
        Node newNode = new Node(data);
        newNode.next = prev.next;
        prev.next = newNode;
        newNode.prev = prev;
        if (null != newNode.next)
            newNode.next.prev = newNode;
        size++;
    }

    public E getFirst() {
        if (0 == size) {
            AlertBox.display("Empty List", "getFirst can not return the first node because the list is empty");
            throw new NoSuchElementException();
        }
        return head.data;
    }

    public Node getFirstNode() {
        if (0 == size) {
            AlertBox.display("Empty List", "getFirst can not return the first node because the list is empty");
            throw new NoSuchElementException();
        }
        return head;
    }

    public E removeFirst() {
        if (0 == size) {
            AlertBox.display("Empty List", "removeFirst can not remove the first node because the list is empty");
            throw new NoSuchElementException();
        }
        Node temp = head;
        delete(head);
        return temp.data;
    }

    public E getLast() {
        if (0 == size) {
            AlertBox.display("Empty List", "getLast() can not return the first node because the list is empty");
            throw new NoSuchElementException();
        }
        return tail.data;
    }

    public Node getLastNode() {
        if (0 == size) {
            AlertBox.display("Empty List", "getLast() can not return the first node because the list is empty");
            throw new NoSuchElementException();
        }
        return tail;
    }

    public E removeLast() {
        if (0 == size) {
            AlertBox.display("Empty List", "removeLast() can not remove the last node because the list is empty");
            throw new NoSuchElementException();
        }
        Node temp = tail;
        delete(tail);
        return temp.data;
    }

    public void delete(Node del) {
        if (null == head || null == del)
            return;
        if (head == del)
            head = del.next;
        if (tail == del)
            tail = del.prev;
        del.next.prev = del.prev;
        del.prev.next = del.next;
        size--;
    }

    public void reverse() {
        if(null == head)
            return;
        Node current = head;
         do {
            Node tempNext = current.next;
            current.next = current.prev;
            current.prev = tempNext;
            current = tempNext;
        } while (head != current);
        Node tempTail = tail;
        tail = head;
        head = tempTail;
    }

    public boolean isBetween(Node between, Node node1, Node node2) {
        return ((between.prev == node1 || between.next == node1) &&
                (between.prev == node2 || between.next == node2));
    }

    public void addAll(List<E> nodes) {
       nodes.forEach(this::append);
    }

    public List<E> getAll() {
        final List<E> returnList = new ArrayList<>();
        if (null == head)
            return returnList;
        Node current = head;
        do {
            returnList.add(current.data);
            current = current.next;
        } while (head != current);

        return returnList;
    }
}
