package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class SpanningTreeTourCalculator extends AbstractRouteCalculator {
    public SpanningTreeTourCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) {
        observableEdges.clear();
        try {
            Vertex startVertex = vertices.get(startNode);
            Map<Vertex, Vertex> parentMap = createParentMap(startVertex);
            if (drawLater(parentMap)) return null;
            List<Vertex> visited = new ArrayList<>();
            Vertex root = getRoot(parentMap);
            visited.add(root);
            preOrderTraversal(parentMap, visited, root);
            if (drawTour(visited)) return null;
            return extractVertexTour(startVertex);
        } catch (Exception e) {
            Platform.runLater(() -> {
                String causeMessage = "cause message not available";
                if (null != e.getCause()) {
                    causeMessage = Objects.requireNonNull(e.getCause()).getLocalizedMessage();
                }
                AlertBox.display("Exception during Spanning Tree Algorithm occured", e.getMessage() + " caused by: " + causeMessage);
            });
        }
        return null;
    }

    private Map<Vertex, Vertex> createParentMap(Vertex startVertex) throws InterruptedException {
        final List<Vertex> copiedVertices = new ArrayList<>(vertices);
        copiedVertices.remove(startVertex);
        Map<Vertex, Double> costMap = new HashMap<>();
        Map<Vertex, Vertex> parentMap = new LinkedHashMap<>();
        costMap.put(startVertex, 0.0);
        parentMap.put(startVertex, null);
        for (Vertex vertex : copiedVertices) {
            costMap.put(vertex, Double.POSITIVE_INFINITY);
            parentMap.put(vertex, null);
        }

        while (0 != costMap.size()) {
            Vertex minVertex = extractMin(costMap);
            costMap.remove(minVertex);
            for (Map.Entry<Vertex, Double> entry : costMap.entrySet()) {
                final double costs = cost(minVertex, entry.getKey());
                if (costs < entry.getValue()) {
                    entry.setValue(costs);
                    costMap.put(entry.getKey(), costs);
                    parentMap.put(entry.getKey(), minVertex);
                }
            }
        }
        return parentMap;
    }

    private Vertex extractMin(Map<Vertex, Double> costMap) {
        return costMap.entrySet().stream()
                .min(Comparator.comparingDouble(Map.Entry::getValue))
                .get()
                .getKey();
    }

    private Vertex getRoot(Map<Vertex, Vertex> parentMap) {
        return parentMap.entrySet().stream()
                .filter(entry -> null == entry.getValue())
                .findFirst()
                .get().getKey();
    }

    private void preOrderTraversal(Map<Vertex, Vertex> parentMap, List<Vertex> visited, Vertex parent) {
        List<Vertex> children = getChildren(parentMap, parent);
        for (Vertex child : children) {
            visited.add(child);
            preOrderTraversal(parentMap, visited, child);
        }
    }

    private List<Vertex> getChildren(Map<Vertex, Vertex> parentMap, Vertex parent) {
        return parentMap.entrySet().stream()
                .filter(entry -> null != entry.getValue())
                .filter(entry -> entry.getValue().equals(parent))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private boolean drawLater(List<Edge> mwpMatching) throws InterruptedException {
        synchronized (observableEdges) {
            observableEdges.clear();
            mwpMatching.forEach(this::drawLater);
            observableEdges.addAll(mwpMatching);
            if (synchronizedPauseOrStop()) return true;
        }
        return false;
    }

    private boolean drawLater(Map<Vertex, Vertex> parentMap) throws InterruptedException {
        List<Edge> parentMapEdges = new ArrayList<>();
        final Map<Vertex, Vertex> parentMapNotNullValues = parentMap.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        parentMapNotNullValues.forEach((key, value) -> parentMapEdges.add(new Edge(key, value)));
        return drawLater(parentMapEdges);
    }

    private boolean drawTour(List<Vertex> visited) throws InterruptedException {
        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < visited.size() - 1; i++) {
            final Edge edge = new Edge(visited.get(i), visited.get(i + 1));
            edges.add(edge);
        }
        edges.add(new Edge(visited.get(visited.size() - 1), visited.get(0)));
        return drawLater(edges);
    }
}
