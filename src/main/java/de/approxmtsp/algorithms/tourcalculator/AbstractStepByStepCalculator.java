package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import javafx.collections.ObservableList;

public abstract class AbstractStepByStepCalculator {
    static boolean paused;
    static boolean stopped;
    private static boolean stepByStep;
    protected final ObservableList<Edge> observableEdges;
    private static boolean animationEnabled;

    public AbstractStepByStepCalculator(ObservableList<Edge> observableEdges) {
        this.observableEdges = observableEdges;
    }

    protected boolean synchronizedPauseOrStop() throws InterruptedException {
        synchronized (observableEdges) {
            if (paused || stepByStep) {
                observableEdges.wait();
            } else if (stopped) {
                return true;
            }
        }
        return false;
    }

    public static void pause() {
        paused = true;
    }

    public void resume() {
        synchronized (observableEdges) {
            paused = false;
            observableEdges.notifyAll();
        }
    }

    public static void stop() {
//        stopped = true;
    }

    public boolean isStopped() {
        return stopped;
    }

    public static boolean isPaused() {
        return paused;
    }

    public static void setStepByStep(boolean stepByStep) {
        AbstractStepByStepCalculator.stepByStep = stepByStep;
    }

    public static boolean isStepByStep() {
        return stepByStep;
    }

    public static void setAnimationEnabled(boolean animationEnabled) {
        AbstractStepByStepCalculator.animationEnabled = animationEnabled;
    }

    public static boolean isAnimationEnabled() {
        return animationEnabled;
    }

    public void clearObservableEdges() {
        observableEdges.clear();
    }
}
