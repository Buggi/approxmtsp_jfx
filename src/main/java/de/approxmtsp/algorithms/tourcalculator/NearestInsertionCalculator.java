package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class NearestInsertionCalculator extends AbstractInsertionCalculator {
    public NearestInsertionCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }
    public List<Vertex> calculate(int startNode) throws InterruptedException {
        return calculateInsertion(startNode, AbstractRouteCalculator::cost, this::updateKey);
    }

    private void updateKey(Map<Vertex, Double> costMap, Vertex vertex, double minValue) {
        if (minValue < costMap.get(vertex)) {
            costMap.put(vertex, minValue);
        }
    }
}
