package de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static de.approxmtsp.algorithms.support.CostCalculator.tourCosts;
import static de.approxmtsp.algorithms.support.FeasibilityChecker.isTourFeasible;
import static de.approxmtsp.algorithms.support.FeasibilityChecker.isUsed;
import static java.util.Arrays.asList;

public class NonSequentialMover extends Mover {

    public NonSequentialMover(List<Edge> allPossibleEdges, List<Vertex> vertices,
                              Map<String, Double> zMap, ObservableList<Edge> observableEdges) {
        super(allPossibleEdges, vertices, zMap, observableEdges);
    }

    public List<Edge> tryNonSequentialMoves(List<Edge> currentTour, List<Edge> currentBestTour) throws InterruptedException {
        List<Edge> tour = new ArrayList<>(currentTour);
        final List<Edge> usedEdges223Moves = new ArrayList<>();
        final List<Edge> usedEdges32Moves = new ArrayList<>();
        final List<Edge> usedX1s = new ArrayList<>(currentBestTour);
        Edge x1;
        List<Edge> tourBefore = new ArrayList<>(tour);
        while (null != (x1 = getX(tour, mergeLists(usedEdges223Moves, usedEdges223Moves, usedX1s)))) {
            double gain = 0;
            //G*
            double gainBest = 0;
            usedEdges223Moves.add(x1);
            usedX1s.add(x1);
            final List<Edge> backupFor32Move = new ArrayList<>(tour);
            //LKH Page 117: Non-sequential moves
            //First option: Any non-sequential 2-opt-move followed by any 2- or 3-opt-move
            gain += nonSequential2OptMove(tour, usedEdges223Moves, x1, false);
            if (!isTourFeasible(tour, vertices)) {
                final List<Edge> backupTour = new ArrayList<>(tour);
                gain += nonSequential2OptMove(tour, usedEdges223Moves, x1, true);
                if (!isTourFeasible(tour, vertices)) {
                    tour = new ArrayList<>(backupTour);//restore changes made by second 2-optMove
                    gain += nonSequential3OptMove(tour, usedEdges223Moves, x1, true);
                }
            }

            //Second option: Any non-sequential 3-opt-move followed by any 2-opt-move
            if (!isTourFeasible(tour, vertices)) {
                gain = 0;
                tour = new ArrayList<>(backupFor32Move);
                gain += nonSequential3OptMove(tour, usedEdges32Moves, x1, false);
                if (!isTourFeasible(tour, vertices)) {
                    gain += nonSequential2OptMove(tour, usedEdges32Moves, x1, true);
                }
            }

            if (isTourFeasible(tour, vertices)) {
                gainBest = Math.max(gain, gainBest);
                if (0 < gainBest && gain == gainBest) {
                    final double currentBestCost = tourCosts(currentTour);
                    final double currentCost = tourCosts(tour);
                    if (currentBestCost <= currentCost) {
                        //TODO: Something went wrong: gainBest is calculated wrong
                        gainBest = 0;
                    } else {
                        currentTour = tour;
                    }
                }
            }
            if (0 == gainBest) {
                tour = new ArrayList<>(tourBefore);
                clearAndDrawLater(tour);
                if (synchronizedPauseOrStop()) return new ArrayList<>();
            } else {
                currentTour = new ArrayList<>(tour);
                //If gain is found, clear x1s and usedEdges223Moves
                usedX1s.clear();
                usedX1s.addAll(currentBestTour);
                usedEdges223Moves.clear();
                usedEdges32Moves.clear();
            }
            tourBefore = new ArrayList<>(tour);
        }
        return currentTour;
    }

    private double nonSequential2OptMove(List<Edge> tour, List<Edge> usedEdges, Edge x1, boolean feasibiltyCheck) {
        final Edge x2 = getX(tour, usedEdges);
        if (null == x2) {
            return Double.NEGATIVE_INFINITY;
        }
        final List<Vertex> freeVertices = new ArrayList<>(asList(x1.getSource(), x1.getTarget(), x2.getSource(), x2.getTarget()));
        final List<Edge> backupTour = new ArrayList<>(tour);
        final List<Edge> allPossibleYs = getAllPossibleYs(freeVertices, usedEdges, tour);
        for (Edge y1 : allPossibleYs) {
            for (Edge y2 : allPossibleYs) {
                if (y1.equals(y2)) continue;
                double gain = calculateGain(asList(x1, x2), asList(y1, y2));
                if (0 < gain) {
                    usedEdges.addAll(asList(x1, x2, y1, y2));
                    makeOptMove(asList(x1, x2), asList(y1, y2), tour);
                    if ((feasibiltyCheck && !isTourFeasible(tour, vertices))) {
                        //undo previous move
                        makeOptMove(asList(y1, y2), asList(x1, x2), tour);
                    }
                    return gain;
                }
                tour = new ArrayList<>(backupTour);
            }
        }
        return 0;
    }

    private double nonSequential3OptMove(List<Edge> tour, List<Edge> usedEdges, Edge x1, boolean feasibilityCheck) {
        final Edge x2 = getX(tour, usedEdges);
        final Edge x3 = getX(tour, usedEdges);
        if (null == x2 || null == x3) {
            return Double.NEGATIVE_INFINITY;
        }
        final List<Vertex> freeVertices = new ArrayList<>(asList(x1.getSource(), x1.getTarget(), x2.getSource(), x2.getTarget(), x3.getSource(), x3.getTarget()));
        final List<Edge> backupTour = new ArrayList<>(tour);
        final List<Edge> allPossibleYs = getAllPossibleYs(freeVertices, usedEdges, tour);
        for (Edge y1 : allPossibleYs) {
            for (Edge y2 : allPossibleYs) {
                for (Edge y3 : allPossibleYs) {
                    if (y1.equals(y2) || y1.equals(y3) || y2.equals(y3)) continue;
                    double gain = calculateGain(asList(x1, x2, x3), asList(y1, y2, y3));
                    if (0 < gain) {
                        usedEdges.addAll(asList(x1, x2, x3, y1, y2, y3));
                        makeOptMove(asList(x1, x2, x3), asList(y1, y2, y3), tour);
                        if (feasibilityCheck && !isTourFeasible(tour, vertices)) {
                            //undo previous move
                            makeOptMove(asList(y1, y2, y3), asList(x1, x2, x3), tour);
                        }
                        return gain;
                    }
                    tour = new ArrayList<>(backupTour);
                }
            }
        }
        return 0;
    }

    private Edge getX(List<Edge> tour, List<Edge> usedEdges) {
        return tour.stream()
                .filter(e -> !isUsed(usedEdges, e))
                .max(Comparator.comparingDouble(e -> zMap.get(e.getId())))
                .orElse(null);
    }

    private List<Edge> getAllPossibleYs(List<Vertex> freeVertices, List<Edge> usedEdges, List<Edge> tour) {
        return freeVertices.stream()//
                .flatMap(v1 -> freeVertices.stream()//
                        .filter(v2 -> !v1.equals(v2)).map(v2 -> new Edge(v1, v2)))//
                .filter(distinctByKey(Edge::getId))//
                .filter(edge -> !isUsed(usedEdges, edge))
                .filter(edge -> !isUsed(tour, edge))
                .sorted(Comparator.comparingDouble(edge -> zMap.get(edge.getId())))
                .collect(Collectors.toList());
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    void clearAndDrawLater(List<Edge> addingEdges) {
        final List<Edge> tempEdges = new ArrayList<>(addingEdges);
        synchronized (observableEdges) {
            observableEdges.clear();
            observableEdges.addAll(tempEdges);
        }
    }

    void drawLater(Color color, Edge add) {
//        final Line line = add.getGraphic();
//        line.setStroke(color);
//        line.setFill(color);
//        add.setLine(line);
//        Platform.runLater(() -> {
//            GraphDrawer.drawEdge(add);
//        });
    }

}
