package de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue;

import de.approxmtsp.algorithms.support.FeasibilityChecker;
import de.approxmtsp.algorithms.tourcalculator.AbstractStepByStepCalculator;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.approxmtsp.algorithms.support.CostCalculator.cost;

public abstract class Mover extends AbstractStepByStepCalculator {
    public static int MAX_Y_TRIES = 5;
    final List<Edge> allPossibleEdges;
    final List<Vertex> vertices;
    final Map<String, Double> zMap;

    public Mover(List<Edge> allPossibleEdges, List<Vertex> vertices,
                 Map<String, Double> zMap, ObservableList<Edge> observableEdges) {
        super(observableEdges);
        this.allPossibleEdges = allPossibleEdges;
        this.vertices = vertices;
        this.zMap = zMap;
    }

    @SafeVarargs
    protected final List<Edge> mergeLists(List<Edge>... lists) {
        return Stream.of(lists)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    void clearAndDrawLater(List<Edge> addingEdges) {
        if (!isAnimationEnabled() && !isStepByStep())
            return;
        final List<Edge> tempEdges = new ArrayList<>(addingEdges);
        synchronized (observableEdges) {
            observableEdges.clear();
            observableEdges.addAll(tempEdges);
        }
    }

    void drawLater(Color color, Edge add) {
        final Line line = add.getGraphic();
        line.setStroke(color);
        line.setFill(color);
        add.setLine(line);
        synchronized (observableEdges) {
            observableEdges.add(add);
        }
    }

    void drawLater(Color color, Edge delete, Edge add) {
        final Line line = add.getGraphic();
        line.setStroke(color);
        line.setFill(color);
        add.setLine(line);
        synchronized (observableEdges) {
            observableEdges.remove(delete);
            observableEdges.add(add);
        }
    }

    void deleteDrawing(Edge delete) {
        observableEdges.remove(delete);
    }

    static double calculateGain(Edge x, Edge y) {
        if (null == x || null == y) {
            return Double.NEGATIVE_INFINITY;
        }
        double costX = cost(x.getSource(), x.getTarget());
        double costY = cost(y.getSource(), y.getTarget());
        return costX - costY;
    }

    static double calculateGain(List<Edge> x, List<Edge> y) {
        double gain = 0;
        if (CollectionUtils.isEmpty(x) || CollectionUtils.isEmpty(y) || x.size() != y.size()) {
            return Double.NEGATIVE_INFINITY;
        }
        for (int i = 0; i < x.size(); i++) {
            gain += calculateGain(x.get(i), y.get(i));
        }
        return gain;
    }

    static void makeOptMove(List<Edge> x, List<Edge> y, List<Edge> tempTour) {
        tempTour.removeAll(x);
        tempTour.addAll(y);
    }

    static boolean validateOptMove(List<Edge> x, List<Edge> y, List<Edge> tour, List<Vertex> vertices) {
        final List<Edge> tempTour = new ArrayList<>(tour);
        tempTour.removeAll(x);
        tempTour.addAll(y);
        return FeasibilityChecker.isTourFeasible(tempTour, vertices);
    }

}
