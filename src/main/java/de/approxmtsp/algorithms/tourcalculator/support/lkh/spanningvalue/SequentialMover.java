package de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.approxmtsp.algorithms.support.CostCalculator.tourCosts;
import static de.approxmtsp.algorithms.support.FeasibilityChecker.isTourFeasible;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class SequentialMover extends Mover {
    private final Map<Integer, List<Edge>> yCandidateMap;
    private List<Edge> xUsed;
    private List<Edge> yUsed;
    private List<Edge> addedEdges;

    public SequentialMover(List<Edge> allPossibleEdges, List<Vertex> vertices,
                           Map<String, Double> zMap, Map<Integer, List<Edge>> yCandidateMap, ObservableList<Edge> observableEdges) {
        super(allPossibleEdges, vertices, zMap, observableEdges);
        this.yCandidateMap = yCandidateMap;
    }

    private Edge getCurrentWorstX1(List<Edge> tour, List<Edge> currBestTour) {
        return tour.stream()
                .filter(edge -> !currBestTour.contains(edge))
                .max(Comparator.comparingDouble(e -> zMap.get(e.getId())))
                .orElse(null);
    }

    public List<Edge> trySequentialMoves(List<Edge> currentTour, List<Edge> currentBestTour) throws InterruptedException {
        List<Edge> tour = new ArrayList<>(currentTour);
        xUsed = new ArrayList<>();
        yUsed = new ArrayList<>();
        addedEdges = new ArrayList<>();
        Edge x1;
        List<Edge> triedX1Edges = new ArrayList<>(currentBestTour);
        while (null != (x1 = getCurrentWorstX1(tour, triedX1Edges))) {
            triedX1Edges.add(x1);
            final List<Edge> sourceYCandidates = getyCandidates(x1.getSource(), x1, tour);
            final List<Edge> targetYCandidates = getyCandidates(x1.getTarget(), x1, tour);
            final List<Edge> yCandidates = Stream.of(sourceYCandidates, targetYCandidates)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            for (Edge y1 : yCandidates) {
                //gi
                double gain = 0;
                //G*
                double gainBest = 0;
                final List<Edge> tempTourBefore = new ArrayList<>(tour);
                xUsed.add(x1);
                yUsed.add(y1);
                addedEdges.add(y1);
                final Vertex t1 = getT1(x1, y1);
                tour.remove(x1);
                tour.add(y1);
                //draw possible move
                if (isStepByStep()) {
                    drawLater(Color.BLUE, x1, y1);
                    drawLater(Color.RED, x1);
                }
                if (synchronizedPauseOrStop()) return new ArrayList<>();

                //includes possible 2-5 OPT moves when gain becomes positive (e.g. 2 OPT has positive gain -> 2 OPT move is executed)
                gain = try2optMove(t1, x1, y1, tour);
                if (isTourFeasible(tour, vertices)) {
                    gainBest = Math.max(gain, gainBest);
                    if (gain == gainBest) {
                        final double currentBestCost = tourCosts(currentTour);
                        final double currentCost = tourCosts(tour);
                        if (currentBestCost <= currentCost) {
                            //TODO: Something went wrong: gainBest is calculated wrong
                            gainBest = 0;
                        } else {
                            currentTour = new ArrayList<>(tour);
                        }
                    }
                }
                if (0 == gainBest) {
                    tour = new ArrayList<>(tempTourBefore);
                    //Edges are not used because sequential moves are reverted --> try with a different x1
                    xUsed.clear();
                    yUsed.clear();
                } else {
                    triedX1Edges = new ArrayList<>(currentBestTour);
                    //edges are used BUT a new tour has been created, so you start with a fresh tour and reset used edges
                    //TODO: If it is ok to clear it regardless which value gain has, then put the clears after the if else clause
                    xUsed.clear();
                    yUsed.clear();

                    //DOUBLE CHECK
                    if (!isTourFeasible(currentTour, vertices)) {
                        Platform.runLater(() -> AlertBox.display("Double Check failed", "CurrentBestTour is not feasible!"));
                    }

                    //cancel y1 search and continue with a new x1
                    break;
                }

            }
            clearAndDrawLater(tour);
            if (synchronizedPauseOrStop()) return new ArrayList<>();
        }
        return currentTour;
    }

    private double try2optMove(Vertex t1, Edge x1, Edge y1, List<Edge> tour) throws InterruptedException {
        double gainMove;
        final Vertex t3 = getT1i(t1, x1, y1);
        final List<Edge> possibleXEdges = getPossibleXEdges(tour, y1, t3);
        for (Edge x2 : possibleXEdges) {
            final Vertex t4 = getT2i(x2, t3);
            final List<Edge> yCandidates = getyCandidates(t4, x2, tour);
            for (Edge y2 : yCandidates) {
                if (t4.equals(t1)) {
                    continue;
                }
                final Edge yGoToT1 = new Edge(t4, t1);
                gainMove = calculateGain(x1, y1);
                final double yToT1Gain = calculateGain(x2, yGoToT1);
                if (isStepByStep()) {
                    drawLater(Color.BLUE, x2, y2);
                    drawLater(Color.RED, x2);
                }
                synchronizedPauseOrStop();
                if (!validateOptMove(singletonList(x2), singletonList(yGoToT1), tour, vertices)) {
                    if (isStepByStep()) {
                        drawLater(Color.GREEN, x2, x2);
                        deleteDrawing(y2);
                    }
                    synchronizedPauseOrStop();
                    continue;
                }
                //(4) disjunctivity criterion: "The last edge to be deleted in a 5-opt-move must not previously have been added in the current chain of 5-opt-moves"
                if (0 >= (gainMove + yToT1Gain) || addedEdges.contains(yGoToT1)) {
                    xUsed.add(x2);
                    yUsed.add(y2);
                    final double gain = try3optMove(t1, t3, x2, y2, tour, gainMove);
                    if (0 < gain) {
                        return gain;
                    }
                    continue;
                }
                if (isStepByStep()) {
                    //Delete y2 and add y that connects to t1
                    drawLater(Color.BLUE, y2, yGoToT1);
                }
                synchronizedPauseOrStop();
                gainMove += yToT1Gain;
                //just x2, and yGoToT1 because x1 and y1 are added in calculate method
                makeOptMove(singletonList(x2), singletonList(yGoToT1), tour);
                addedEdges.add(yGoToT1);
                clearAndDrawLater(tour);
                synchronizedPauseOrStop();
                return gainMove;
            }

        }
        if (isStepByStep()) {
            deleteDrawing(y1);
        }
        synchronizedPauseOrStop();
        //no gain found
        return 0;
    }

    private double try3optMove(Vertex t1, Vertex t3, Edge x2, Edge y2, List<Edge> tour, double gainOpt)
            throws InterruptedException {
        final Vertex t5 = getT1i(t3, x2, y2);
        final List<Edge> possibleXEdges = getPossibleXEdges(tour, y2, t5);
        for (Edge x3 : possibleXEdges) {
            final Vertex t6 = getT2i(x3, t5);
            final List<Edge> yCandidates = getyCandidates(t6, x3, tour);
            for (Edge y3 : yCandidates) {
                if (t6.equals(t1)) {
                    continue;
                }
                final Edge yGoToT1 = new Edge(t6, t1);
                final double backupGain = gainOpt;
                gainOpt += calculateGain(x2, y2);
                final double yToT1Gain = calculateGain(x3, yGoToT1);
                if (isStepByStep()) {
                    drawLater(Color.BLUE, x3, y3);
                    drawLater(Color.RED, x3);
                }
                synchronizedPauseOrStop();
                if (!validateOptMove(asList(x2, x3), asList(y2, yGoToT1), tour, vertices)) {
                    if (isStepByStep()) {
                        drawLater(Color.GREEN, x3, x3);
                        deleteDrawing(y3);
                    }
                    synchronizedPauseOrStop();
                    gainOpt = backupGain;
                    continue;
                }
                if (0 >= (gainOpt + yToT1Gain) || addedEdges.contains(yGoToT1)) {
                    xUsed.add(x3);
                    yUsed.add(y3);
                    final double gain = try4optMove(t1, t5, x2, y2, x3, y3, tour, gainOpt);
                    if (0 < gain) {
                        return gain;
                    }
                    gainOpt = backupGain;
                    continue;
                }
                if (isStepByStep()) {
                    drawLater(Color.BLUE, y3, yGoToT1);
                }
                synchronizedPauseOrStop();
                gainOpt += yToT1Gain;
                //just x2, and yGoToT1 because x1 and y1 are added in calculate method
                makeOptMove(asList(x2, x3), asList(y2, yGoToT1), tour);
                addedEdges.addAll(asList(y2, yGoToT1));
                clearAndDrawLater(tour);
                synchronizedPauseOrStop();
                return gainOpt;
            }
        }
        //no gain found
        return 0;
    }

    private double try4optMove(Vertex t1, Vertex t5, Edge x2, Edge y2, Edge x3, Edge y3, List<Edge> tour, double gainOpt)
            throws InterruptedException {
        final Vertex t7 = getT1i(t5, x3, y3);
        final List<Edge> possibleXEdges = getPossibleXEdges(tour, y3, t7);
        for (Edge x4 : possibleXEdges) {
            final Vertex t8 = getT2i(x4, t7);
            final List<Edge> yCandidates = getyCandidates(t8, x3, tour);
            for (Edge y4 : yCandidates) {
                if (t8.equals(t1)) {
                    continue;
                }
                final Edge yGoToT1 = new Edge(t8, t1);
                final double backupGain = gainOpt;
                gainOpt += calculateGain(x3, y3);
                final double yToT1Gain = calculateGain(x4, yGoToT1);
                if (isStepByStep()) {
                    drawLater(Color.BLUE, x4, y4);
                    drawLater(Color.RED, x4);
                }
                synchronizedPauseOrStop();
                if (!validateOptMove(asList(x2, x3, x4), asList(y2, y3, yGoToT1), tour, vertices)) {
                    if (isStepByStep()) {
                        drawLater(Color.GREEN, x4, x4);
                        deleteDrawing(y4);
                    }
                    synchronizedPauseOrStop();
                    gainOpt = backupGain;
                    continue;
                }
                if (0 >= (gainOpt + yToT1Gain) || addedEdges.contains(yGoToT1)) {
                    xUsed.add(x4);
                    yUsed.add(y4);
                    final double gain = try5optMove(t1, t7, x2, y2, x3, y3, x4, y4, tour, gainOpt);
                    if (0 < gain) {
                        return gain;
                    }
                    gainOpt = backupGain;
                    continue;
                }
                if (isStepByStep()) {
                    drawLater(Color.BLUE, y4, yGoToT1);
                }
                synchronizedPauseOrStop();
                gainOpt += yToT1Gain;
                //just x2, and yGoToT1 because x1 and y1 are added in calculate method
                makeOptMove(asList(x2, x3, x4), asList(y2, y3, yGoToT1), tour);
                addedEdges.addAll(asList(y2, y3, yGoToT1));
                clearAndDrawLater(tour);
                synchronizedPauseOrStop();
                return gainOpt;
            }
        }
        //no gain found
        return 0;
    }


    private double try5optMove(Vertex t1, Vertex t7, Edge x2, Edge y2, Edge x3, Edge y3, Edge x4, Edge y4,
                               List<Edge> tour, double gainOpt) throws InterruptedException {
        final Vertex t9 = getT1i(t7, x3, y3);
        final List<Edge> possibleXEdges = getPossibleXEdges(tour, y4, t9);
//        final List<Edge> possibleEdges = mergeLists(possibleXEdges, possibleY5Edges);
        for (Edge x5 : possibleXEdges) {
            final Vertex t10 = getT2i(x5, t9);
            if (t10.equals(t1)) {
                continue;
            }
            final Edge y5 = new Edge(t10, t1);
            final Edge yGoToT1 = new Edge(t10, t1);
            final double backupGain = gainOpt;
            gainOpt += calculateGain(x4, y4);
            final double yToT1Gain = calculateGain(x5, yGoToT1);
            if (isStepByStep()) {
                drawLater(Color.BLUE, x5, y5);
                drawLater(Color.RED, x5);
            }
            synchronizedPauseOrStop();
            if (!validateOptMove(asList(x2, x3, x4, x5), asList(y2, y3, y4, yGoToT1), tour, vertices)) {
                if (isStepByStep()) {
                    drawLater(Color.GREEN, x5, x5);
                    deleteDrawing(y5);
                }
                synchronizedPauseOrStop();
                gainOpt = backupGain;
                continue;
            }
            if (0 >= (gainOpt + yToT1Gain)) {
                gainOpt = backupGain;
                continue;
            }
            if (isStepByStep()) {
                drawLater(Color.BLUE, y5, yGoToT1);
            }
            synchronizedPauseOrStop();
            gainOpt += yToT1Gain;
            //just x2, and yGoToT1 because x1 and y1 are added in calculate method
            makeOptMove(asList(x2, x3, x4, x5), asList(y2, y3, y4, yGoToT1), tour);
            addedEdges.addAll(asList(y2, y3, y4, yGoToT1));
            clearAndDrawLater(tour);
            synchronizedPauseOrStop();
            return gainOpt;
        }
        //no gain found
        return 0;
    }

    private List<Edge> getyCandidates(Vertex t4, Edge xi, List<Edge> tour) {
        return yCandidateMap.get(t4.getId()).stream()
                .filter(edge -> !edge.equals(xi))
//                .filter(edge -> !xUsed.contains(edge))
                .filter(edge -> !yUsed.contains(edge))
//                .filter(edge -> !usedEdges.contains(edge))
                .filter(edge -> !tour.contains(edge))
                .collect(Collectors.toList());
    }

    private List<Edge> getPossibleXEdges(List<Edge> tempTour, Edge yLast, Vertex t1i) {
        return tempTour.stream()
                //at this point yUsed does not cointan yLast, so this check is necessary
                .filter(edge -> !yLast.equals(edge))
                .filter(edge -> !xUsed.contains(edge))
                .filter(edge -> !yUsed.contains(edge))
                .filter(edge -> edge.getSource().equals(t1i) || edge.getTarget().equals(t1i))
                .sorted(Comparator.comparingDouble(edge -> zMap.get(((Edge) edge).getId())).reversed())
                .collect(Collectors.toList());
    }

    private static Vertex getT1(Edge x1, Edge y1) {
        List<Vertex> yVertices = asList(y1.getSource(), y1.getTarget());
        return yVertices.contains(x1.getSource()) ? x1.getTarget() : x1.getSource();
    }

    private static Vertex getT1i(Vertex t1Before, Edge x, Edge y) {
        final Vertex t2 = t1Before.equals(x.getSource()) ? x.getTarget() : x.getSource();
        return y.getSource().equals(t2) ? y.getTarget() : y.getSource();
    }

    private static Vertex getT2i(Edge x, Vertex t1i) {
        return x.getSource().equals(t1i) ? x.getTarget() : x.getSource();
    }


}
