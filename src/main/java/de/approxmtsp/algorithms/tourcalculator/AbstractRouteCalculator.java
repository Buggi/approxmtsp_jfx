package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.algorithms.RouteCalculatorException;
import de.approxmtsp.algorithms.support.CostCalculator;
import de.approxmtsp.controller.GraphDrawer;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public abstract class AbstractRouteCalculator extends AbstractStepByStepCalculator {
    final List<Vertex> vertices;
    int countVertices;
    double lowestDistance;
    static final Logger LOGGER = Logger.getLogger(AbstractRouteCalculator.class.getName());


    static de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap heap = new de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap();

    public AbstractRouteCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(observableEdges);
        Validate.notNull(vertices, "vertices is null");
        Validate.notNull(observableEdges, "observableEdges is null");
        this.vertices = new ArrayList<>(vertices);
        countVertices = vertices.size();
        initializeLogger();
    }

    private void initializeLogger() throws IOException {
        final FileHandler logFile = new FileHandler("log.txt", true);
        logFile.setFormatter(new SimpleFormatter());
        LOGGER.addHandler(logFile);
    }

    public abstract List<Vertex> calculate(int startNode) throws RouteCalculatorException, InterruptedException;

    // works with Lin Kernighan Spanning Tree Algorithm
    List<Vertex> extractVertexTour(Vertex startVertex) {
        List<Edge> tempEdges = new ArrayList<>(observableEdges);
        List<Vertex> finalTour = new ArrayList<>();
        finalTour.add(startVertex);
        Optional<Edge> nextEdge = findNextEdge(tempEdges, startVertex);

        while (finalTour.size() < observableEdges.size()) {
            if (nextEdge.isPresent()) {
                Edge edge = nextEdge.get();
                tempEdges.remove(edge);
                Vertex nextVertex = finalTour.contains(edge.getTarget()) ? edge.getSource() : edge.getTarget();
                finalTour.add(nextVertex);
                nextEdge = findNextEdge(tempEdges, nextVertex);
            } else {
//                Platform.runLater(() -> AlertBox.display("Could not extract tour", "Could not extract tour"));
                throw new RouteCalculatorException("Could not extract tour");
            }
        }

        return finalTour;
    }

    List<Edge> convertToEdgeTour(List<Vertex> tour) {
        final List<Edge> edgeTour = new ArrayList<>();
        for (int i = 0; i < tour.size() - 1; i++) {
            edgeTour.add(new Edge(tour.get(i), tour.get(i + 1)));
        }
        edgeTour.add(new Edge(tour.get(tour.size() - 1), tour.get(0)));
        return edgeTour;
    }

    Optional<Edge> findNextEdge(List<Edge> tempEdges, Vertex lastVertex) {
        return tempEdges.stream()
                .filter(edge -> edge.getSource().getId() == lastVertex.getId() || edge.getTarget().getId() == lastVertex.getId())
                .findFirst();
    }

    protected boolean synchronizedPauseOrStop() throws InterruptedException {
        synchronized (observableEdges) {
            if (paused || isStepByStep()) {
                observableEdges.wait();
            } else if (stopped) {
                return true;
            }
        }
        return false;
    }

    public static double cost(Vertex src, Vertex dest) {
        return CostCalculator.cost(src, dest);
    }

    public static double cost(Edge edge) {
        return CostCalculator.cost(edge);
    }

    void drawLater(Edge delete, Edge addFirst, Edge addSecond) {
        Platform.runLater(() -> {
            GraphDrawer.deleteEdge(delete);
            GraphDrawer.drawEdge(addFirst);
            GraphDrawer.drawEdge(addSecond);
        });
    }

    void drawLater(Edge delete, Edge... addingEdges) {
        Platform.runLater(() -> {
            GraphDrawer.deleteEdge(delete);
            Arrays.stream(addingEdges).forEach(GraphDrawer::drawEdge);
        });
    }

    void drawLater(Edge edge) {
        Platform.runLater(() -> GraphDrawer.drawEdge(edge));
    }

    void drawLater(Color color, Edge delete, Edge add) {
        final Line line = add.getGraphic();
        line.setStroke(color);
        line.setFill(color);
        add.setLine(line);
        Platform.runLater(() -> {
            GraphDrawer.deleteEdge(delete);
            GraphDrawer.drawEdge(add);
        });
    }

    void drawLater(Color color, Edge add) {
        final Line line = add.getGraphic();
        line.setStroke(color);
        line.setFill(color);
        add.setLine(line);
        Platform.runLater(() -> {
            GraphDrawer.drawEdge(add);
        });
    }

    void drawLater(List<Edge> deletingEdges, List<Edge> addingEdges) {
        Platform.runLater(() -> {
            deletingEdges.forEach(GraphDrawer::deleteEdge);
            deletingEdges.forEach(edge -> drawLater(Color.RED, edge));
            addingEdges.forEach(edge -> drawLater(Color.BLUE, edge));
        });
    }

    void clearAndDrawLater(List<Edge> addingEdges) {
        final List<Edge> tempEdges = new ArrayList<>(addingEdges);
        Platform.runLater(() -> {
            GraphDrawer.removeAllEdges();
            tempEdges.forEach(edge -> drawLater(Color.GREEN, edge));
        });
    }
}
