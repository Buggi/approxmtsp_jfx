package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DoubleEndedNearestNeighbourCalculator extends AbstractRouteCalculator {
    private double lowestDistanceFirstEnd;
    private double lowestDistanceSecondEnd;

    public DoubleEndedNearestNeighbourCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    public List<Vertex> calculate(int startNode) throws InterruptedException {
        observableEdges.clear();
        final List<Vertex> copiedVertices = new ArrayList<>(vertices);
        LinkedList<Vertex> tour = new LinkedList<>();
        Vertex startVertex = copiedVertices.get(startNode);
        Vertex currVertexFirstEnd = startVertex;
        Vertex currVertexSecondEnd = startVertex;
        Vertex nextVertexFirstEnd;
        Vertex nextVertexSecondEnd;
        while (tour.size() < countVertices) {
            if (synchronizedPauseOrStop()) break;
            lowestDistanceFirstEnd = Double.POSITIVE_INFINITY;
            lowestDistanceSecondEnd = Double.POSITIVE_INFINITY;
            nextVertexFirstEnd = null;
            nextVertexSecondEnd = null;
            for (Vertex vertex : copiedVertices) {
                double costs;
                if ((costs = cost(currVertexFirstEnd, vertex)) < lowestDistanceFirstEnd) {
                    lowestDistanceFirstEnd = costs;
                    nextVertexFirstEnd = vertex;
                }
                if ((costs = cost(currVertexSecondEnd, vertex)) < lowestDistanceSecondEnd) {
                    lowestDistanceSecondEnd = costs;
                    nextVertexSecondEnd = vertex;
                }
            }
            synchronized (observableEdges) {
                if (lowestDistanceFirstEnd < lowestDistanceSecondEnd) {
                    observableEdges.add(new Edge(currVertexFirstEnd, nextVertexFirstEnd));
                    tour.add(0, nextVertexFirstEnd);
                    copiedVertices.remove(nextVertexFirstEnd);
                    currVertexFirstEnd = nextVertexFirstEnd;
                } else {
                    observableEdges.add(new Edge(currVertexSecondEnd, nextVertexSecondEnd));
                    tour.add(nextVertexSecondEnd);
                    copiedVertices.remove(nextVertexSecondEnd);
                    currVertexSecondEnd = nextVertexSecondEnd;
                }
            }
        }
        synchronized (observableEdges) {
            if (paused || isStepByStep()) observableEdges.wait();
            observableEdges.add(new Edge(currVertexFirstEnd, currVertexSecondEnd));
        }
        return tour;
    }
}
