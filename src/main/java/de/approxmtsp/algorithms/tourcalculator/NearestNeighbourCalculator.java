package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NearestNeighbourCalculator extends AbstractRouteCalculator {


    public NearestNeighbourCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    public List<Vertex> calculate(int startNode) throws InterruptedException {
        final List<Vertex> copiedVertices = new ArrayList<>(vertices);
        observableEdges.clear();
        List<Vertex> tour = new ArrayList<>();
        //List<Edge> edges = new ArrayList<>();
        Vertex startVertex = copiedVertices.get(startNode);
        Vertex currVertex = startVertex;
        Vertex nextVertex;
        while (tour.size() < countVertices - 1) {
            if (synchronizedPauseOrStop()) break;
//                    updateProgress(tour.size(), countVertices);
            tour.add(currVertex);
            vertices.remove(currVertex);
            lowestDistance = Double.POSITIVE_INFINITY;
            nextVertex = null;
            for (Vertex vertex : copiedVertices) {
                double costs;
                if ((costs = cost(currVertex, vertex)) < lowestDistance) {
                    lowestDistance = costs;
                    nextVertex = vertex;
                }
            }
            synchronized (observableEdges) {
                observableEdges.add(new Edge(currVertex, nextVertex));
            }
            currVertex = nextVertex;
        }
        tour.add(currVertex);

        synchronized (observableEdges) {
            if (paused || isStepByStep()) {
                observableEdges.wait();
            }
            observableEdges.add(new Edge(currVertex, startVertex));
        }
        return tour;
    }
}
