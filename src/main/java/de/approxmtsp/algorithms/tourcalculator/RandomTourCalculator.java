package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.algorithms.RouteCalculatorException;
import de.approxmtsp.algorithms.support.FeasibilityChecker;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomTourCalculator extends AbstractRouteCalculator{
    public RandomTourCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) throws RouteCalculatorException {
        observableEdges.clear();
        final List<Vertex> tempVertices = new ArrayList<>(vertices);
        final List<Vertex> tour = new ArrayList<>();
        while (!tempVertices.isEmpty()) {
            final Vertex removedVertex = tempVertices.remove(getRandomNumber(tempVertices.size()));
            tour.add(removedVertex);
        }
        final List<Edge> edgeTour = convertToEdgeTour(tour);
        if (!FeasibilityChecker.isTourFeasible(edgeTour, vertices)){
            Platform.runLater(() -> AlertBox.display("Random tour is not feasible", "Random tour is not feasible"));
        }
        observableEdges.addAll(edgeTour);
        return tour;
    }

    private int getRandomNumber(int size) {
        if (1 == size) {
            return 0;
        }
        return new Random().nextInt(size - 1);
    }
}
