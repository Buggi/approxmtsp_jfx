package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.io.IOException;
import java.util.*;

abstract class AbstractInsertionCalculator extends AbstractRouteCalculator {
    BidiMap<Vertex, Vertex> nextVertexTourMap;

    AbstractInsertionCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    interface CostInterface {
        double cost(Vertex v1, Vertex v2);
    }

    interface UpdateKey {
        void updateKey(Map<Vertex, Double> costMap, Vertex vertex, double newCosts);
    }


    List<Vertex> calculateInsertion(int startNode, CostInterface costInterface, UpdateKey updateKeyInterface) throws InterruptedException {
        List<Vertex> copiedVertices = new ArrayList<>(vertices);
        observableEdges.clear();
        nextVertexTourMap = new DualHashBidiMap<>();
        Vertex startVertex = copiedVertices.get(startNode);
        synchronized (observableEdges) {
            observableEdges.add(new Edge(startVertex, startVertex));
        }
        copiedVertices.remove(startVertex);
        Map<Vertex, Double> costMap = new HashMap<>();
        for (Vertex vertex : copiedVertices) {
            costMap.put(vertex, costInterface.cost(vertex, startVertex));
        }
        while (observableEdges.size() < countVertices) {
            if (synchronizedPauseOrStop()) break;
            Vertex minVertex = costMap.entrySet().stream()
                    .min(Comparator.comparingDouble(Map.Entry::getValue))
                    .get()
                    .getKey();


            synchronized (observableEdges) {
                insertIntoTour(minVertex);
                copiedVertices.remove(minVertex);
                costMap.remove(minVertex);
                //does not work anymore, because the Task is not created in this class anymore
//                        updateProgress(observableEdges.size(), countVertices);
            }
            costMap.forEach((key, value) -> {
                double minValue = Math.min(value, costInterface.cost(key, minVertex));
                updateKeyInterface.updateKey(costMap, key, minValue);
            });
        }
        return extractVertexTour(startVertex);
    }

    private void insertIntoTour(Vertex vertex) {
//        1: (a) Finde Kante (i, j) in der Tour T, sodass c(i,k)+c(k,j)-c(i,j) minimal ist.
        double minCosts = Double.POSITIVE_INFINITY;
        Edge edgeDelete = null;
        for (Edge edge : observableEdges) {
            double insertionCosts = cost(edge.getSource(), vertex) + cost(vertex, edge.getTarget()) - cost(edge.getSource(), edge.getTarget());
            if (minCosts > insertionCosts) {
                minCosts = insertionCosts;
                edgeDelete = edge;
            }
        }
//        2: (b) Lösche Kante (i; j) aus T und füge die Kanten (i;k) und (k; j) hinzu.
        if (null != edgeDelete) {
            observableEdges.remove(edgeDelete);
            final Edge edge1 = new Edge(edgeDelete.getSource(), vertex);
            final Edge edge2 = new Edge(vertex, edgeDelete.getTarget());
            observableEdges.add(edge1);
            observableEdges.add(edge2);
            nextVertexTourMap.put(edge1.getSource(), edge1.getTarget());
            nextVertexTourMap.put(edge2.getSource(), edge2.getTarget());
        }

    }
}
