package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.algorithms.RouteCalculatorException;
import de.approxmtsp.algorithms.support.ZMapCalculator;
import de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue.Mover;
import de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue.NonSequentialMover;
import de.approxmtsp.algorithms.tourcalculator.support.lkh.spanningvalue.SequentialMover;
import de.approxmtsp.controller.GraphDrawer;
import de.approxmtsp.controller.StageController;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static de.approxmtsp.algorithms.support.CostCalculator.tourCosts;
import static de.approxmtsp.algorithms.support.CostCalculator.tourCostsRounded;
import static de.approxmtsp.algorithms.support.FeasibilityChecker.isTourFeasible;
import static de.approxmtsp.controller.StageController.getCurrentGraphName;
import static java.lang.Math.sqrt;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

public class LinKernighanSpanningTreeValueTourCalculator extends AbstractRouteCalculator {
    public static int MAX_TRIES = 10;
    //    private final FarthestInsertionCalculator initialCalculator;
    private final AbstractRouteCalculator initialCalculator;

    public LinKernighanSpanningTreeValueTourCalculator(List<Vertex> vertices, ObservableList<Edge> tourEdges) throws IOException, InterruptedException {
        super(vertices, tourEdges);
        initialCalculator = new FarthestInsertionCalculator(vertices, tourEdges);
//        initialCalculator = new RandomTourCalculator(vertices, tourEdges);
        if (isEmpty(tourEdges)) {
            initialCalculator.calculate(new Random().nextInt(vertices.size() - 1));
        }
    }

    @Override
    public List<Vertex> calculate(int startNode) throws RouteCalculatorException, InterruptedException {
        LOGGER.info("Started optimizing with Lin-Kernighan-Helsgaun-Spanning-Value for " + StageController.getCurrentGraphName());
        final List<Edge> initialTour = new ArrayList<>(observableEdges);
        LOGGER.info("initial costs: " + tourCostsRounded(initialTour));
        final List<Edge> allPossibleEdges = getAllPossibleEdges();
        final ZMapCalculator zMapCalculator = new ZMapCalculator(allPossibleEdges, this.vertices);
        final Map<String, Double> zMap = zMapCalculator.createZMap();
        List<Edge> currentTour = new ArrayList<>(initialTour);
        final Map<Integer, List<Edge>> yCandidates = vertices.stream()
                .collect(Collectors.toMap(Vertex::getId, vertex -> getYCandidates(vertex, initialTour, allPossibleEdges, zMap)));
        final SequentialMover sm = new SequentialMover(allPossibleEdges, this.vertices, zMap, yCandidates, observableEdges);
        final NonSequentialMover nsm = new NonSequentialMover(allPossibleEdges, this.vertices, zMap, observableEdges);

        List<Edge> bestTour = new ArrayList<>();
        List<Edge> currentBestTour = new ArrayList<>();
        observableEdges.clear();
        for (int i = 0; i < MAX_TRIES; i++) {
            double currentTourCost = tourCosts(currentTour);
            double currentBestTourCosts = Double.POSITIVE_INFINITY;
            while (currentTourCost < currentBestTourCosts) {
                currentBestTourCosts = currentTourCost;
                //if it is the first try don't set currentBestTour
                if (currentBestTourCosts != Double.POSITIVE_INFINITY) {
                    currentBestTour = new ArrayList<>(currentTour);
                }
                LOGGER.info("trying sequential moves");
                currentTour = sm.trySequentialMoves(currentTour, bestTour);
                LOGGER.info("costs: " + tourCostsRounded(currentTour));
                LOGGER.info("trying non sequential moves");
                currentTour = nsm.tryNonSequentialMoves(currentTour, bestTour);
                LOGGER.info("costs: " + tourCostsRounded(currentTour));
                currentTourCost = tourCosts(currentTour);
            }
            if (tourCosts(currentBestTour) < tourCosts(bestTour) && isTourFeasible(currentBestTour, vertices)) {
                bestTour = new ArrayList<>(currentBestTour);
                if (isTourOptimal(bestTour)) {
                    optimalAlert();
                    break;
                }
            }
            initialCalculator.calculate(new Random().nextInt(vertices.size() - 1));
            currentTour = new ArrayList<>(observableEdges);
        }
        if (bestTour.isEmpty()) {
            Platform.runLater(() -> AlertBox.display("bestTour shouldn't be empty", "currentBestTour shouldnt be empty"));
            bestTour = new ArrayList<>(currentTour);
        }
        synchronized (observableEdges) {
            observableEdges.clear();
            observableEdges.addAll(bestTour);
        }
        if (!isTourFeasible(bestTour, vertices)) {
            Platform.runLater(() -> AlertBox.display("Final Tour is not feasible", "Final Tour is not feasible"));
        }
        final List<Vertex> currentTourVertices = extractVertexTour(this.vertices.get(startNode), currentBestTour);
        createTourFile(currentTourVertices, tourCosts(bestTour), tourCostsRounded(bestTour), getCurrentGraphName());
        return extractVertexTour(vertices.get(startNode), bestTour);
    }

    private void optimalAlert() {
        Platform.runLater(() -> {
            final String message = "Optimal solution is found after non sequential moves and MAX_Y_TRIES = " + Mover.MAX_Y_TRIES;
            AlertBox.display("OPT is found", message);
            LOGGER.info(message);
        });
        return;
    }

    private List<Vertex> extractVertexTour(Vertex start, List<Edge> tour) {
        final List<Edge> tempTour = new ArrayList<>(tour);
        final List<Vertex> vertexTour = new ArrayList<>();
        vertexTour.add(start);
        Optional<Edge> nextEdge = findNextEdge(tempTour, start);
        while (vertexTour.size() < tour.size()) {
            if ((!nextEdge.isPresent())) {
                throw new RouteCalculatorException("Could not extract tour");
            }
            final Edge edge = nextEdge.get();
            tempTour.remove(edge);
            final Vertex nextVertex = vertexTour.contains(edge.getTarget()) ? edge.getSource() : edge.getTarget();
            vertexTour.add(nextVertex);
            nextEdge = findNextEdge(tempTour, nextVertex);
        }
        return vertexTour;
    }

    private boolean isTourOptimal(List<Edge> bestTour) {
        final double costsRounded = getCostsRounded(bestTour);
        return GraphDrawer.getOptimum() >= costsRounded;
    }

    private double getCostsRounded(List<Edge> bestTour) {
        return bestTour.stream()
                .mapToDouble(edge -> Math.round(euclideanDistance(edge.getSource(), edge.getTarget())))
                .sum();
    }

    private double euclideanDistance(Vertex v1, Vertex v2) {
        double xPowed = Math.pow(v2.getX() - (v1.getX()), 2);
        double yPowed = Math.pow(v2.getY() - (v1.getY()), 2);

        return sqrt(xPowed + yPowed);
    }

    private List<Edge> getAllPossibleEdges() {
        return vertices.stream()
                .flatMap(v1 -> vertices.stream()
                        .filter(v2 -> !v1.equals(v2)).map(v2 -> new Edge(v1, v2)))
                .filter(distinctByKey(Edge::getId))
                .collect(Collectors.toList());
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private List<Edge> getYCandidates(Vertex v, List<Edge> initialTour, List<Edge> allPossibleEdges, Map<String, Double> zMap) {
        return allPossibleEdges.stream()
                .filter(edge -> edge.getSource().equals(v) || edge.getTarget().equals(v))
                .filter(edge -> !initialTour.contains(edge))
                .sorted(Comparator.comparingDouble(e -> zMap.get(e.getId())))
                .limit(Mover.MAX_Y_TRIES)
                .collect(Collectors.toList());
    }

    private void createTourFile(List<Vertex> vertices, double calculationCosts, double calculationCostsRounded, String graphName) {
        PrintWriter writer = null;
        try {
            final String cleanGraphName = graphName.substring(0, graphName.lastIndexOf("."));
            final File lkhsvtours = new File("lkhsvtours");
            if (createTourDirectory(lkhsvtours)) return;
            File file = new File(lkhsvtours, cleanGraphName + "_LKHSV_Y_CANDIDATES_" + Mover.MAX_Y_TRIES + "_COSTS_" + calculationCostsRounded + ".tour");
            writer = new PrintWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(writer, "writer is null");
        writer.println("NAME: " + graphName);
        writer.println("COMMENT: Length = " + calculationCosts);
        writer.println("COMMENT: Length (rounded) = " + calculationCostsRounded);
        writer.println("TYPE: TOUR");
        writer.println("DIMENSION: " + vertices.size());
        writer.println("TOUR_SECTION");
        PrintWriter finalWriter = writer;
        vertices.forEach(vertex -> finalWriter.println(vertex.getId()));
        writer.println("-1");
        writer.println("EOF");
        writer.close();
    }

    private boolean createTourDirectory(File lkhsvTours) {
        if (!lkhsvTours.exists()) {
            try {
                lkhsvTours.mkdir();
            } catch (SecurityException e) {
                Platform.runLater(() -> AlertBox.display("Could not create directory", "Could not create directory " + lkhsvTours.getName() + "\r" + e.getMessage()));
                return true;
            }
        }
        return false;
    }

}
