package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.algorithms.heapimplementations.fiedler.FibonacciHeap;
import de.approxmtsp.controller.GraphDrawer;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ChristofidesCalculator extends AbstractRouteCalculator {

    public ChristofidesCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) throws InterruptedException {
        observableEdges.clear();
        Vertex startVertex = vertices.get(startNode);
        Map<Vertex, Vertex> parentMap = createParentMap(startVertex);
        if (updateTour(parentMap)) return null;
        List<Edge> mst = createMst(parentMap);
        List<Vertex> oddVerticesInMst = getOddVertices(mst);
        List<Edge> mwpMatching = new ArrayList<>();
        Vertex startMwpmVertex = oddVerticesInMst.get(0);
        //TODO: create minimum weight maximum matching
        createMatching(oddVerticesInMst, mwpMatching, startMwpmVertex);
        if (updateTour(mwpMatching)) return null;
        validateMatching(oddVerticesInMst, mwpMatching);
        List<Vertex> eulerCircle = createEulerCircle(mst, mwpMatching, startVertex);
        final List<Vertex> hamiltonCircle = createHamiltonCircle(eulerCircle);
        if (drawTour(hamiltonCircle)) return null;
        return hamiltonCircle;
    }

    private boolean updateTour(List<Edge> mwpMatching) throws InterruptedException {
        synchronized (observableEdges) {
            observableEdges.clear();
            observableEdges.addAll(mwpMatching);
            if (synchronizedPauseOrStop()) return true;

        }
        return false;
    }

    private boolean updateTour(Map<Vertex, Vertex> parentMap) throws InterruptedException {
        List<Edge> parentMapEdges = new ArrayList<>();
        final Map<Vertex, Vertex> parentMapNotNullValues = parentMap.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        parentMapNotNullValues.forEach((key, value) -> parentMapEdges.add(new Edge(key, value)));
        return updateTour(parentMapEdges);
    }

    private void validateMatching(List<Vertex> oddVerticesInMst, List<Edge> mwpMatching) {
        if (oddVerticesInMst.size() / 2 != mwpMatching.size()) {
            System.out.println("not a perfect matching: vertices.size() / 2 = " + oddVerticesInMst.size() / 2 + "mwpMatching.size() = " + mwpMatching.size());
            AlertBox.display("Error Christofides", "not a perfect matching: vertices.size() / 2 = " + oddVerticesInMst.size() / 2 + "mwpMatching.size() = " + mwpMatching.size());
        }
    }

    private Map<Vertex, Vertex> createParentMap(Vertex startVertex) throws InterruptedException {
        heap.clear();
        final List<Vertex> copiedVertices = new ArrayList<>(vertices);
        copiedVertices.remove(startVertex);
        Map<FibonacciHeap.Node, Double> costMap = new HashMap<>();
        Map<Vertex, Vertex> parentMap = new LinkedHashMap<>();
//        TreeMap<Vertex,Vertex> parentMap = new TreeMap<>();
        heap.insert(startVertex, 0);
        parentMap.put(startVertex, null);
        for (Vertex vertex : copiedVertices) {
            FibonacciHeap.Node node = heap.insert(vertex, cost(startVertex, vertex));
            costMap.put(node, Double.POSITIVE_INFINITY);
            parentMap.put(vertex, null);
        }
        FibonacciHeap.Node minNode = null;
        while (0 != heap.size()) {
            final Vertex lastMinVertex = null != minNode ? (Vertex) minNode.getObject() : null;
            minNode = heap.removeMin();
            costMap.remove(minNode);
            for (Map.Entry<FibonacciHeap.Node, Double> entry : costMap.entrySet()) {
                final double costs = cost((Vertex) minNode.getObject(), (Vertex) entry.getKey().getObject());
                if (costs < entry.getValue()) {
                    entry.setValue(costs);
                    heap.decreaseKey(entry.getKey(), costs);
                    parentMap.put((Vertex) entry.getKey().getObject(), (Vertex) minNode.getObject());
                }
            }
        }
        return parentMap;
    }

    private List<Edge> createMst(Map<Vertex, Vertex> parentMap) {
        return parentMap.entrySet().stream()
                .filter(entry -> entry.getKey() != null)
                .filter(entry -> entry.getValue() != null)
                .map(entry -> new Edge(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private List<Vertex> getOddVertices(List<Edge> mst) {
        List<Vertex> sourceVertices = mst.stream()
                .map(Edge::getSource)
                .collect(Collectors.toList());
        List<Vertex> targetVertices = mst.stream()
                .map(Edge::getTarget)
                .collect(Collectors.toList());

        List<Vertex> sourceAndTarget = new ArrayList<>(sourceVertices);
        sourceAndTarget.addAll(targetVertices);

        return sourceAndTarget.stream()
                .collect(Collectors.groupingBy(v -> v, Collectors.counting())).entrySet()
                .stream()
                .filter(entry -> entry.getValue() % 2 != 0)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private void createMatching(List<Vertex> oddVerticesInMst, List<Edge> mwpMatching, Vertex startMwpmVertex) throws InterruptedException {
        Vertex matchingVertex = findMinimumMatchingVertex(oddVerticesInMst, mwpMatching, startMwpmVertex);
        final Edge edge = new Edge(startMwpmVertex, matchingVertex);
        mwpMatching.add(edge);

        Optional<Vertex> nextFreeVertex = oddVerticesInMst.stream()
                .filter(vertex -> !isVertexInMatching(vertex, mwpMatching))
                .findFirst();
        nextFreeVertex.ifPresent(vertex -> {
            try {
                createMatching(oddVerticesInMst, mwpMatching, vertex);
            } catch (InterruptedException e) {
                e.printStackTrace();
                AlertBox.display("Step-By-Step Error during Christofides Algorithm", "Message: " + e.getMessage());
            }
        });

    }

    private Vertex findMinimumMatchingVertex(List<Vertex> oddVerticesInMst, List<Edge> mwpMatching, Vertex startMwpmVertex) {
        return oddVerticesInMst.stream()
                .filter(vertex -> !vertex.equals(startMwpmVertex))
                .filter(vertex -> !isVertexInMatching(vertex, mwpMatching))
                .min(Comparator.comparingDouble(vertex -> cost(startMwpmVertex, vertex)))
                .get();
    }

    private boolean isVertexInMatching(Vertex vertex, List<Edge> matching) {
        return matching.stream()
                .anyMatch(edge -> edge.getSource().equals(vertex) || edge.getTarget().equals(vertex));
    }

    private List<Vertex> createEulerCircle(List<Edge> mst, List<Edge> mwpMatching, Vertex startVertex) throws InterruptedException {
        List<Edge> merged = new LinkedList<>();
        merged.addAll(mst);
        merged.addAll(mwpMatching);

        Optional<Edge> startEdgeOptional = merged.stream()
                .filter(edge -> startVertex.getId() == edge.getSource().getId() || startVertex.getId() == edge.getTarget().getId())
                .findFirst();

        if (!startEdgeOptional.isPresent()) {
            AlertBox.display("Error Christofides", "Can't find start edge");
            return null;
        }

        Edge nextEdge = startEdgeOptional.get();
        final List<Vertex> eulerTour = createSubTour(merged, startVertex, nextEdge);
        return eulerTour;
    }

    private List<Vertex> createSubTour(List<Edge> merged, Vertex subTourStart, Edge nextEdge) throws InterruptedException {
        Vertex nextVertex;
        LinkedList<Vertex> subTour = new LinkedList<>();
        subTour.add(subTourStart);
        merged.remove(nextEdge);
        nextVertex = nextEdge.getSource() != subTourStart ? nextEdge.getSource() : nextEdge.getTarget();
        subTour.add(nextVertex);
        while (!subTourStart.equals(nextVertex)) {
            Vertex finalNextVertex = nextVertex;
            final Optional<Edge> nextEdgeOpt = findNextEdge(merged, finalNextVertex);
            if (!nextEdgeOpt.isPresent()) break;
            nextEdge = nextEdgeOpt.get();
            nextVertex = nextEdge.getSource() != nextVertex ? nextEdge.getSource() : nextEdge.getTarget();
            subTour.add(nextVertex);
            merged.remove(nextEdge);
        }

        //ADD next sub tour to current sub tour
        for (int i = 0; i < subTour.size(); i++) {
            Vertex vertex = subTour.get(i);
            final Optional<Edge> optionalEdge = findNextEdge(merged, vertex);
            //check for other subTours
            if (optionalEdge.isPresent()) {
                List<Vertex> nextSubTour = createSubTour(merged, vertex, optionalEdge.get());
                subTour.addAll(i + 1, nextSubTour);
            }
        }
        return subTour;
    }

    private List<Vertex> createHamiltonCircle(List<Vertex> eulerCircle) {
        List<Vertex> hamiltonCircle =
                eulerCircle.stream()
                        .distinct()
                        .collect(Collectors.toList());
        return hamiltonCircle;
    }

    private boolean drawTour(List<Vertex> visited) throws InterruptedException {
        Platform.runLater(GraphDrawer::removeAllEdges);
        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < visited.size() - 1; i++) {
            final Edge edge = new Edge(visited.get(i), visited.get(i + 1));
            edges.add(edge);
        }
        edges.add(new Edge(visited.get(visited.size() - 1), visited.get(0)));
        return updateTour(edges);
    }
}

