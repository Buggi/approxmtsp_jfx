package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.io.IOException;
import java.util.*;

public class FarthestInsertionLowestDistanceMaxCalculator extends AbstractRouteCalculator {
    private BidiMap<Vertex, Vertex> nextVertexTourMap;

    public FarthestInsertionLowestDistanceMaxCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) throws InterruptedException {
        final List<Vertex> copiedVertices = new ArrayList<>(vertices);
        observableEdges.clear();
        nextVertexTourMap = new DualHashBidiMap<>();
        Vertex startVertex = copiedVertices.get(startNode);
        synchronized (observableEdges) {
            observableEdges.add(new Edge(startVertex, startVertex));
        }
        copiedVertices.remove(startVertex);
        Map<Vertex, Double> costMap = new HashMap<>();
        for (Vertex vertex : copiedVertices) {
            costMap.put(vertex, cost(vertex, startVertex));
        }
        while (observableEdges.size() < countVertices) {
            if (synchronizedPauseOrStop()) break;

            Vertex maxVertex = costMap.entrySet().stream()
                    .max(Comparator.comparingDouble(Map.Entry::getValue))
                    .get()
                    .getKey();

            synchronized (observableEdges) {
                insertIntoTour(maxVertex);
                copiedVertices.remove(maxVertex); //TODO; remove this?
                costMap.remove(maxVertex);
//                        updateProgress(observableEdges.size(), countVertices);
            }
            costMap.forEach((key, value) -> {
                final double costToAddedVertex = cost(key, maxVertex);
                double minValue = Math.min(value, costToAddedVertex);
                if (minValue < value) {
                    costMap.put(key, minValue);
                }
            });
        }
        return extractVertexTour(startVertex);
    }
//        };
//    }

    private void insertIntoTour(Vertex vertex) {
//        1: (a) Finde Kante (i, j) in der Tour T, sodass c(i,k)+c(k,j)-c(i,j) minimal ist.
        double minCosts = Double.POSITIVE_INFINITY;
        Edge edgeDelete = null;
        for (Edge edge : observableEdges) {
            double insertionCosts = cost(edge.getSource(), vertex) + cost(vertex, edge.getTarget()) - cost(edge.getSource(), edge.getTarget());
            if (minCosts > insertionCosts) {
                minCosts = insertionCosts;
                edgeDelete = edge;
            }
        }
//        2: (b) Lösche Kante (i; j) aus T und füge die Kanten (i;k) und (k; j) hinzu.
        if (null != edgeDelete) {
            observableEdges.remove(edgeDelete);
            final Edge edge1 = new Edge(edgeDelete.getSource(), vertex);
            final Edge edge2 = new Edge(vertex, edgeDelete.getTarget());
            observableEdges.add(edge1);
            observableEdges.add(edge2);
            nextVertexTourMap.put(edge1.getSource(), edge1.getTarget());
            nextVertexTourMap.put(edge2.getSource(), edge2.getTarget());
        }

    }
}
//
//public class FarthestInsertionLowestDistanceMaxCalculator extends AbstractInsertionCalculator {
//    public FarthestInsertionLowestDistanceMaxCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) {
//        super(vertices, observableEdges);
//    }
//
//    @Override
//    public Task<List<Vertex>> calculate(int startNode) {
//        return calculateInsertion(startNode, ((v1, v2) -> cost(v1, v2)), this::updateKey);
//    }
//
//    private void updateKey(Map<FibonacciHeap.Node, Double> costMap, FibonacciHeap.Node entry, double newCosts) {
//        //da es negiert ist, muss geprüft werden ob die neuen Kosten größer sind
//        //wenn der aktuell geprüfte Knoten näher zu dem neu hinzugefügten Knoten ist, braucht der Kostenwert ein Update
//        if (newCosts > entry.getKey()) {
//            costMap.put(entry, newCosts);
////            heap.decreaseKey(entry, newCosts); //Exception cannot increase Key
//            heap.delete(entry);
//            heap.insert(entry.getObject(), newCosts);
//        }
//    }
//}
