package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CheapestInsertionCalculator extends AbstractInsertionCalculator {
    public CheapestInsertionCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) throws InterruptedException {
        return calculateInsertion(startNode, this::calculateInsertionCosts, this::updateKey);
    }

    private double calculateInsertionCosts(Vertex vertex, Vertex minVertex) {
        if(vertices.size() > countVertices - 2) {
            return AbstractRouteCalculator.cost(minVertex, vertex);
        } else {
            List<Vertex> prevAndNextVertex = getNextAndPreviousVertex(minVertex);
            double costsEdgeA = cost(minVertex, vertex) + cost(vertex, prevAndNextVertex.get(0)) - cost(minVertex, prevAndNextVertex.get(0));
            double costsEdgeB = cost(minVertex, vertex) + cost(vertex, prevAndNextVertex.get(1)) - cost(minVertex, prevAndNextVertex.get(1));
            return Math.min(costsEdgeA, costsEdgeB);
        }
    }

    private List<Vertex> getNextAndPreviousVertex(Vertex vertex) {
        final Vertex next = nextVertexTourMap.get(vertex);
        final Vertex prev = nextVertexTourMap.getKey(vertex);
        return Arrays.asList(prev, next);
    }

    private void updateKey(Map<Vertex, Double> costMap, Vertex vertex, double minValue) {
        if (minValue < costMap.get(vertex)) {
            costMap.put(vertex, minValue);
        }
    }

//    private void updateKey(Map<FibonacciHeap.Node, Double> costMap, FibonacciHeap.Node entry, double minValue) {
//        heap.decreaseKey(entry, minValue);
//        costMap.put(entry, minValue);
//    }

}
