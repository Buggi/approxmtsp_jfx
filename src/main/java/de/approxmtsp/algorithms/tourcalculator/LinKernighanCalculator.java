package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.algorithms.RouteCalculatorException;
import de.approxmtsp.algorithms.listimplementations.DoublyLinkedList;
import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import de.approxmtsp.ui.AlertBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.*;

public class LinKernighanCalculator extends AbstractRouteCalculator {

    private final AbstractRouteCalculator calculator;
    private Map<Edge, Double> tourEdgesCosts = new HashMap<>();
    private List<Edge> tourEdges;
    private List<Edge> usedEdges = new ArrayList<>();
    private List<Edge> tempTour;
    private Map<List<Edge>, Double> tempTours = new HashMap<>();
    private List<Edge> used;
    private List<Vertex> initialTour;
    private double gain;
    private double gainBest;
    private List<Edge> bestTour;
    private List<Edge> triedX1s = new ArrayList<>();
    private List<Edge> beforeY2TempTour;

    public LinKernighanCalculator(List<Vertex> vertices, ObservableList<Edge> tourEdges) throws IOException {
        super(vertices, tourEdges);
       // calculator = new FarthestInsertionLowestDistanceMaxCalculator(vertices, tourEdges);
        calculator = new SpanningTreeTourCalculator(vertices, tourEdges);
        this.tourEdges = tourEdges;
    }

    @Override
    public List<Vertex> calculate(int startNode) throws InterruptedException, RouteCalculatorException {
        observableEdges.clear();
        initialTour = calculator.calculate(startNode);
        tourEdges.forEach(edge -> tourEdgesCosts.put(edge, cost(edge.getSource(), edge.getTarget())));
        final DoublyLinkedList<Vertex> initialDLLTour = new DoublyLinkedList<>();
        initialDLLTour.addAll(initialTour);
        tempTour = new ArrayList<>(tourEdges);
        ArrayList<Vertex> triedT1s = new ArrayList<>();
        do {
            //gi
            gain = 0;
            //G*
            gainBest = 0;
            int i = 0;
            List<Edge> tempTourBefore = tempTour;
            Vertex t1;
            if (triedT1s.isEmpty()) {
//                t1 = initialDLLTour.getFirst();
                t1 = initialTour.get(0);
            } else {
                t1 = initialTour.stream()
                        .filter(v -> !triedT1s.contains(v))
                        .findFirst().orElse(null);
            }
            //TODO: x1 and y1 "most out of place" pair
            Edge x1 = getX1(t1);
            Edge y1 = getY1(x1, t1);
            triedX1s.add(x1);
            while (null == y1) {
                x1 = getAlternativeX1(t1, triedX1s);
                triedX1s.add(x1);
                y1 = getY1(x1, t1);
            }
            tempTour.remove(x1);
            tempTour.add(y1);
            drawLater(x1, y1);
            if (synchronizedPauseOrStop()) return new ArrayList<>();
            gain += calculateGain(x1, y1);
            //updateGainBestAndBestTour();
            tempTours.put(tempTour, gain);
            i++;
//            beforeY2TempTour = tempTour;
            findRemainingYs(t1, y1, i);
            if (0 == gainBest) {
                tempTour = tempTourBefore;
                triedT1s.add(t1);
            }
        } while (gainBest >= 0 && triedT1s.size() < initialTour.size()); //5. last sentence
        return null;
    }

    private Edge getX1(Vertex t1) throws RouteCalculatorException {
        Optional<Edge> x1Opt = tourEdges.stream()
                .filter(edge -> (edge.getSource().getId() == t1.getId()) || edge.getTarget().getId() == t1.getId())
                //TODO: take edge with max costs
                .findFirst();
        if(!x1Opt.isPresent()) {
            AlertBox.display("Lin-Kernighan error", "Could not determine x1");
            throw new RouteCalculatorException("Lin-Kernighan error: Could not determine x1");
        }
        return x1Opt.get();
    }

    private Edge getAlternativeX1(Vertex t1, List<Edge> triedX1s) throws RouteCalculatorException {
        Optional<Edge> x1Opt = tourEdges.stream()
                .filter(edge -> (edge.getSource().getId() == t1.getId()) || edge.getTarget().getId() == t1.getId())
                .filter(edge -> !triedX1s.contains(edge))
                //TODO: take edge with max costs
                .findFirst();
        if(!x1Opt.isPresent()) {
            AlertBox.display("Lin-Kernighan error", "Could not determine x1");
            throw new RouteCalculatorException("Lin-Kernighan error: Could not determine x1");
        }
        return x1Opt.get();
    }

    private Edge getY1(Edge x1, Vertex t1) throws RouteCalculatorException {
        final Vertex t2 = getT2(x1, t1);
        Optional<Vertex> nearestVertex = vertices.stream()
                .filter(v -> v.getId() != t2.getId())
                .filter(v -> !isUsed(tourEdges, t2, v))
                .filter(v -> !isUsed(usedEdges, t2, v))
                .filter(v -> 0 < calculateGain(x1, new Edge(t2, v)))
                .max(Comparator.comparingDouble(v -> (calculateGain(x1, new Edge(t2, v)))));
//                .min(Comparator.comparingDouble(v -> cost(t2, v))); //Why min costs of y1?
        return nearestVertex.map(vertex -> new Edge(t2, vertex)).orElse(null);
    }

    private Vertex getT2(Edge x1, Vertex t1) {
        return x1.getSource().equals(t1) ? x1.getTarget() : x1.getSource();
    }


    /**
     * @param x Current edge x
     * @param t1i current t1
     * @return null if no gaining would be negative when adding any y
     *          OR y which does not let gain be negative
     */
    private Edge getY(Edge x, Vertex t1i) {
        final Vertex t2i = getT2(x, t1i);
        double tempGain = gain;
        List<Vertex> tempVertices = new ArrayList<>(vertices);
        Optional<Vertex> nearestVertex = tempVertices.stream()
                .filter(v -> v.getId() != t2i.getId())
                .filter(v -> !isUsed(tourEdges, t2i, v))
                .filter(v -> !isUsed(usedEdges, t2i, v))
                .filter(v -> 0 < (tempGain + calculateGain(x, new Edge(t2i, v))))//macht dies und die nächste Zeile die Ausführung von 6(a) irrelevant?: nein, da es entscheidend ist ob am Ende G* != 0 ist
                .max(Comparator.comparingDouble(v -> (tempGain + calculateGain(x, new Edge(t2i, v)))));
//                .min(Comparator.comparingDouble(v -> cost(t2i, v))); //Why min costs of y1?

        return nearestVertex.map(vertex -> new Edge(t2i, vertex)).orElse(null);
    }

    private void findRemainingYs(Vertex t1, Edge y1, int i) throws InterruptedException {
        Edge y = null;
        //see getY1(...): "t3" is always the target in the returned edge
        Vertex t1i = y1.getTarget();
        Edge x = null;
        Edge[] possibleX;
        do {
            //Step 4
            //a)
            final Vertex finalT1i = t1i;
            possibleX = tourEdges.stream()
                    .filter(edge -> (edge.getSource().getId() == finalT1i.getId()) || edge.getTarget().getId() == finalT1i.getId())
                    .toArray(Edge[]::new);

            //6(b)
            if (null == y && 2 == i && null != x) {
               x = x.equals(possibleX[0]) ? possibleX[1] : possibleX[0];
               
            }
            else if (isFeasible(t1i, possibleX[0], t1)) {
                x = possibleX[0];
            }
            else if (isFeasible(t1i, possibleX[1], t1)) {
                x = possibleX[1];
            } else {
                // Shouldn't reach this point here because "The choice of yi-1 (Step 4(e), ensures, that there is always such an xi"
                Platform.runLater(() -> AlertBox.display("No Feasible X found", "No Feasible X found"));
                return;
            }
            usedEdges.add(x);
            //b)
            tempTour.remove(x);
            y = getY(x, t1i);

            //TODO: refactor into method
            final Vertex t2i = getT2(x, t1i);
            final Edge yGoToT1 = new Edge(t2i, t1);
            final double gainXT1 = calculateGain(x, yGoToT1);
            gain += gainXT1;
            if (0 > gain)
                break;
            tempTour.add(yGoToT1);
            drawLater(x, yGoToT1);
            updateGainBestAndBestTour();
            //f)-start
            if (null != y) {
                //remove current "goBackToT1" - edge from further gain calculation
                tempTour.remove(yGoToT1);
                gain -= gainXT1;
                t1i = y.getTarget();
                final double gainXY = calculateGain(x, y);
                gain += gainXY;
                tempTour.add(y);
                drawLater(yGoToT1, y);
            //f)-end
            }
            if (synchronizedPauseOrStop()) break;

            //also 6(b) to try again in step i=2 with breaking the feasibility criterion --> try the other edge for x2
            if (null != y) {
                i++;
            }
            //d)
        } while (null != y || 2 == i); //no gain found
    }


    private boolean isUsed(List<Edge> edges, Edge edge) {
        return isUsed(edges, edge.getSource(), edge.getTarget());
    }

    private boolean isUsed(List<Edge> edges, Vertex v1, Vertex v2) {
        Optional<Edge> optEdge = edges.stream()
                .filter(edge -> (edge.getSource().getId() == v1.getId() && edge.getTarget().getId() == v2.getId())
                        ||      (edge.getSource().getId() == v2.getId() && edge.getTarget().getId() == v1.getId()))
                .findFirst();

        return optEdge.isPresent();
    }

    private double calculateGain(Edge x, Edge y) {
        double costX = cost(x.getSource(), x.getTarget());
        double costY = cost(y.getSource(), y.getTarget());
        double gain = costX - costY;
        return gain;
    }

    private boolean isFeasible(Vertex t1i, Edge possibleX, Vertex t1) {
        if (isUsed(usedEdges, possibleX))
            return false;
        Vertex t2i = possibleX.getSource().getId() == t1i.getId() ? possibleX.getTarget() : possibleX.getSource();
        tempTour.remove(possibleX);
        Edge yGoBackToT1 = new Edge(t2i, t1);
        tempTour.add(yGoBackToT1);

        Edge prev = null;
        Edge start = tempTour.get(0);
        Optional<Edge> currEdge = findNextEdge(start, prev);

        prev = start;
        if (!currEdge.isPresent()) {
            undoFeasibleCheckChanges(possibleX, yGoBackToT1, tempTour);
            return false;
        }
        int size = 2;
        while (size < tourEdges.size()) {
            if(start.equals(currEdge.get())) {
                undoFeasibleCheckChanges(possibleX, yGoBackToT1, tempTour);
                return false;
            }
            Optional<Edge> nextEdge = findNextEdge(currEdge.get(), prev);
            if(!nextEdge.isPresent()) {
                AlertBox.display("Lin-Kerninghan-Error", "Could not find next Edge");
                undoFeasibleCheckChanges(possibleX, yGoBackToT1, tempTour);
                return false;
            }
            size++;
            prev = currEdge.get();
            currEdge = nextEdge;
        }
        undoFeasibleCheckChanges(possibleX, yGoBackToT1, tempTour);
        return true;
    }

    private void undoFeasibleCheckChanges(Edge possibleX, Edge yGoBackToT1, List<Edge> tempTour) {
        tempTour.add(possibleX);
        tempTour.remove(yGoBackToT1);
    }

    private Optional<Edge> findNextEdge(Edge curr, Edge previous) {
        int currSrcId = curr.getSource().getId();
        int currTgtId = curr.getTarget().getId();
        return tempTour.stream()
                .filter(edge -> edge.getSource().getId() == currSrcId || edge.getSource().getId() == currTgtId
                            ||  edge.getTarget().getId() == currSrcId || edge.getTarget().getId() == currTgtId)
                .filter(edge -> !edge.equals(previous))
                .filter(edge -> !edge.equals(curr))
                .findFirst();
    }
    private void updateGainBestAndBestTour() {
        if (gain > gainBest) {
            gainBest = gain;
            bestTour = tempTour;
            tempTours.put(tempTour, gain);
        }
    }
}
