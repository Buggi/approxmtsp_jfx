package de.approxmtsp.algorithms.tourcalculator;

import de.approxmtsp.graph.Edge;
import de.approxmtsp.graph.Vertex;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FarthestInsertionCalculator extends AbstractInsertionCalculator {
    public FarthestInsertionCalculator(List<Vertex> vertices, ObservableList<Edge> observableEdges) throws IOException {
        super(vertices, observableEdges);
    }

    @Override
    public List<Vertex> calculate(int startNode) throws InterruptedException {
        return calculateInsertion(startNode, ((v1, v2) -> -cost(v1, v2)), (costMap, vertex, newCosts) -> updateKey(costMap, vertex, newCosts));
    }

    private void updateKey(Map<Vertex, Double> costMap, Vertex vertex, double newCosts) {
        //da es negiert ist, muss geprüft werden ob die neuen Kosten größer sind
        //wenn der aktuell geprüfte Knoten näher zu dem neu hinzugefügten Knoten ist, braucht der Kostenwert ein Update
        if (newCosts > costMap.get(vertex)) {
            costMap.put(vertex, newCosts);
        }
    }
//    private void updateKey(Map<FibonacciHeap.Node, Double> costMap, FibonacciHeap.Node entry, double newCosts) {
//        //da es negiert ist, muss geprüft werden ob die neuen Kosten größer sind
//        //wenn der aktuell geprüfte Knoten näher zu dem neu hinzugefügten Knoten ist, braucht der Kostenwert ein Update
//        if (newCosts > entry.getKey()) {
//            costMap.put(entry, newCosts);
////            heap.decreaseKey(entry, newCosts); //Exception cannot increase Key
//            heap.delete(entry);
//            heap.insert(entry.getObject(), newCosts);
//        }
//    }
}
