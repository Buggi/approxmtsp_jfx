package de.approxmtsp.graph;

public class EdgePair {
  private Edge x;
  private Edge y;

  public EdgePair(Edge x, Edge y) {
    this.x = x;
    this.y = y;
  }

  public Edge getX() {
    return x;
  }

  public Edge getY() {
    return y;
  }
}
