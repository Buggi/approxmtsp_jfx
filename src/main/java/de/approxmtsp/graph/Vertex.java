package de.approxmtsp.graph;

import de.approxmtsp.controller.GraphDrawer;
import de.approxmtsp.controller.StageController;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Vertex{


    private double radius = 2;
    private int id;
    private Circle circle = null;
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }


    public Vertex(int id, double x, double y) {
        this.id = id;
        this.x = x;
        this.y = y;

    }
    public Circle getGraphic() {
        if(null == circle) {
            circle = new Circle();
            circle.setRadius(radius);
            //circle.setStroke(Color.RED);
            circle.setFill(Color.RED);
            circle.setCenterX(x);
            circle.setCenterY(y);
//            circle.setOnMouseClicked(event -> AlertBox.display(id+"", id+""));
            circle.setOnMouseClicked(event -> {
                GraphDrawer.getVertices().forEach(vertex -> vertex.getGraphic().setFill(Color.RED));
                this.circle.setFill(Color.BLACK);
                StageController.setCurrentStartVertex(this.id - 1);
            });
        }

        return circle;
    }

    public int getId() {
        return id;
    }

    public  void setRadius(double radius) {
        this.radius = radius;
        circle.setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Vertex.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Vertex other = (Vertex) obj;

        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

}
