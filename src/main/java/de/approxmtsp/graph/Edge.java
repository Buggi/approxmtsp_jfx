package de.approxmtsp.graph;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.Objects;

public class Edge {
    private final Vertex source;
    private final Vertex target;
    private static final double STROKEWIDTH = 2.0;
    private Line line = null;
    private String id;

    public Edge(Vertex source, Vertex target) {
        Objects.requireNonNull(source, "Source of Edge must not be null");
        Objects.requireNonNull(target, "Target of Edge must not be null");
        // check again for farthest insertion. somehow this algorithm needs this state
        //if (source.equals(target))
        //    throw new IllegalArgumentException("Edge is not allowed to have same source and target");
        this.source = source;
        this.target = target;
        if (source.getId() < target.getId()) {
            id = source.getId() + "-" + target.getId();
        } else {
            id = target.getId() + "-" + source.getId();
        }
    }

    public Vertex getSource() {
        return source;
    }

    public Vertex getTarget() {
        return target;
    }

    public Line getGraphic() {
        if(null == line) {
            line = new Line();

            line.setStroke(Color.GREEN);
            line.setFill(Color.GREEN);
            line.setStartX(source.getGraphic().getCenterX());
            line.setStartY(source.getGraphic().getCenterY());
            line.setEndX(target.getGraphic().getCenterX());
            line.setEndY(target.getGraphic().getCenterY());
        }
        line.setStrokeWidth(STROKEWIDTH);
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Edge.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Edge other = (Edge) obj;
        // return (this.source != other.source || this.target != other.target) &&
        //        (this.source != other.target || this.target != other.source);

        return (this.source.equals(other.source) && this.target.equals(other.target)) ||
                (this.source.equals(other.target) && this.target.equals(other.source));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.source != null ? this.source.hashCode() : 0);
        hash = 53 * hash + (this.target != null ? this.target.hashCode() : 0);
        return hash;
    }

    public String getId() {
        return id;
    }
}
