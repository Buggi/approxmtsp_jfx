package de.approxmtsp.algorithms.listimplementations;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DoublyLinkedListTest {

    @Test
    public void testThatIsEmptyReturnsTrue() {
        final DoublyLinkedList list = new DoublyLinkedList();

        assertTrue(list.isEmpty());
    }

    @Test
    public void testThatIsEmptyReturnsFalse() {
        final DoublyLinkedList<Double> list = new DoublyLinkedList<>();

        list.append(1.0);

        assertFalse(list.isEmpty());
    }

    @Test
    public void testThatPushInsertsAnObjectInFirstPosition() {
        final int pushObject = 0;
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);

        list.push(pushObject);

        assertEquals(pushObject, (int) list.getFirst());
    }

    @Test
    public void testThatAppendInsertsAnObjectInLastPosition() {
        final int appendObject = 3;
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.push(2);
        list.push(1);

        list.append(appendObject);

        assertEquals(appendObject, (int) list.getLast());
    }

    @Test
    public void testThatTailNextIsHead() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);
        final DoublyLinkedList<java.lang.Integer>.Node first = list.getFirstNode();
        final DoublyLinkedList<java.lang.Integer>.Node last = list.getLastNode();

        assertEquals((Integer) 1, first.data);
        assertEquals((Integer) 3, last.data);
        assertEquals(first, last.next);
    }

    @Test
    public void testThatHeadPrevIsTail() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);
        final DoublyLinkedList<java.lang.Integer>.Node first = list.getFirstNode();
        final DoublyLinkedList<java.lang.Integer>.Node last = list.getLastNode();

        assertEquals((Integer) 1, first.data);
        assertEquals((Integer) 3, last.data);
        assertEquals(last.data, first.prev.data);
    }

    @Test
    public void testThatInsertAfterInsertsAnObjectAfterAnSpecificObject() {
        final int insertObject = 2;
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(3);
        final DoublyLinkedList<Integer>.Node prev = list.getFirstNode();

        list.insertAfter(prev, insertObject);

        assertEquals(insertObject, (int) list.getFirstNode().next.data);
    }

    @Test
    public void testThatDeleteDeletesAnNode() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(0);
        list.append(2);
        final DoublyLinkedList<Integer>.Node firstNode = list.getFirstNode();

        list.delete(firstNode.next);

        assertEquals(2, list.size());
    }

    @Test
    public void testThatDeleteDeletesDoesNotDestroyTheList() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(0);
        list.append(2);
        final DoublyLinkedList<Integer>.Node firstNode = list.getFirstNode();

        list.delete(firstNode.next);

        assertEquals(list.getLastNode(), list.getFirstNode().next);
        assertEquals(list.getFirstNode(), list.getLastNode().next);
    }

    @Test
    public void testThatReverseReversesTheOrderOfTheList() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);

        list.reverse();
        final DoublyLinkedList<Integer>.Node firstNode = list.getFirstNode();
        final String expectedOrder = "321";
        final String order = getContentOfList(firstNode);

        assertEquals(expectedOrder, order);
    }

    @Test
    public void testThatLastNodeCanBeRemoved() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);

        list.removeLast();

        assertEquals(2, (int) list.getLast());
    }

    @Test
    public void testThatWhenTailIsRemovedCircularityIsStillGiven() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);

        list.removeLast();
        final DoublyLinkedList<java.lang.Integer>.Node first = list.getFirstNode();
        final DoublyLinkedList<java.lang.Integer>.Node last = list.getLastNode();

        assertEquals((Integer) 1, first.data);
        assertEquals((Integer) 2, last.data);
        assertEquals(first, last.next);
        assertEquals(last, first.prev);
    }

    @Test
    public void testThatFirstNodeCanBeRemoved() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);

        list.removeFirst();

        assertEquals(2, (int) list.getFirst());
    }

    @Test
    public void testThatIsBetweenReturnsTrueIfTheGivenNodeIsBetweenTheGivenNodes() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        final DoublyLinkedList<java.lang.Integer>.Node before = list.getLastNode();
        list.append(2);
        final DoublyLinkedList<java.lang.Integer>.Node between = list.getLastNode();
        list.append(3);
        final DoublyLinkedList<java.lang.Integer>.Node after = list.getLastNode();

        final boolean isBetween = list.isBetween(between, before, after);

        assertTrue(isBetween);
    }

    @Test
    public void testThatIsBetweenReturnsTrueIfTheGivenNodeIsTheLastAndAfterNodeIsTheFirst() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        final DoublyLinkedList<java.lang.Integer>.Node after = list.getLastNode();
        list.append(2);
        final DoublyLinkedList<java.lang.Integer>.Node before = list.getLastNode();
        list.append(3);
        final DoublyLinkedList<java.lang.Integer>.Node between = list.getLastNode();

        final boolean isBetween = list.isBetween(between, before, after);


        assertTrue(isBetween);
    }

    @Test
    public void testThatIsBetweenReturnsFalseIfTheGivenNodeIsNotBetweenTheGivenNodes() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        final DoublyLinkedList<java.lang.Integer>.Node after = list.getLastNode();
        list.append(2);
        final DoublyLinkedList<java.lang.Integer>.Node before = list.getLastNode();
        list.append(3);
        final DoublyLinkedList<java.lang.Integer>.Node between = list.getLastNode();
        list.append(4);

        final boolean isBetween = list.isBetween(between, before, after);

        assertFalse(isBetween);
    }

    @Test
    public void testThatAddAllIncreasesTheSizeOfTheList() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.append(1);
        list.append(2);
        final int sizeBefore = list.size();

        list.addAll(Arrays.asList(3, 4, 5));
        final int sizeAfter = list.size();

        assertTrue(sizeAfter > sizeBefore);
        assertEquals(5, sizeAfter);
    }

    @Test
    public void testThatAddAllAddsAllElementsToTheList() {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        final List<Integer> elementsList = Arrays.asList(1, 2, 3, 4, 5);

        list.addAll(elementsList);
        final List<Integer> elementsGet = list.getAll();

        assertFalse(elementsGet.isEmpty());
        assertTrue(elementsList.containsAll(elementsGet));
    }

    private String getContentOfList(DoublyLinkedList<Integer>.Node firstNode) {
        StringBuilder order = new StringBuilder();
        DoublyLinkedList<java.lang.Integer>.Node current = firstNode;
        do {
            order.append(current.data);
            current = current.next;
        } while (!firstNode.equals(current));
        return order.toString();
    }
}